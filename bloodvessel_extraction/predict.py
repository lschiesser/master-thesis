import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import torch
from PIL import Image
import torchvision.transforms as T
import segmentation_models_pytorch as smp
import argparse
from collections import OrderedDict
import numpy as np

parser = argparse.ArgumentParser(description="Blood Vessel Segmentation Single Image")
parser.add_argument('-i', '--image', type=str, help="Path to image")

args = parser.parse_args()

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

path = args.image

image = Image.open(path)

augs_resize = T.Compose([
    T.Resize((512, 512)),
    T.ToTensor(),
])

model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=2
)
new_state_dict = OrderedDict()
checkpoint = torch.load('outputs/bloodvessel-crop/best_model.pth', map_location=torch.device("cpu"))
new_state_dict = OrderedDict()
for k, v in checkpoint.state_dict().items():
    if k.find(".fc.") == -1:
        if k[:7] == 'module.':
            name = k[7:]  # remove module.
        else:
            name = k
        new_state_dict[name] = v
model.load_state_dict(new_state_dict)
model.to(device)
model.eval()

image = augs_resize(image)
image = torch.unsqueeze(image, dim=0)

image = image.to(device)
pred = model(image)
pred = torch.argmax(pred, 1)

def prepare_plot(origImage, predMask):
    figure, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 15))

    origImage = np.transpose(origImage.squeeze(0), (1, 2, 0))
    predMask = predMask.squeeze(0)
    ax[0].imshow(origImage)
    ax[1].imshow(origImage)
    ax[1].imshow(predMask, cmap=ListedColormap(['#ffffff00', 'white']))
    figure.tight_layout()
    plt.show()

prepare_plot(image, pred)
