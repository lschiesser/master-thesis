import torch
from torch.utils import data
from PIL import Image
import numpy as np
import torchvision.transforms as T
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from util.identify_papillae import identify_papillae


class DriveDataSet(data.Dataset):
    def __init__(self,
                 imagepath,
                 targetpath,
                 transforms=None,
                 torch_transforms=None,
                 crop=False,
                 nr_transforms=4):
        self.images = []
        self.targets = []
        self.transforms = transforms
        self.torch_transforms = torch_transforms
        self.dtype = torch.float32
        self.nr_transforms = nr_transforms
        self.to_tensor = T.ToTensor()

        self.imagepaths = list(sorted(imagepath))
        self.targetpaths = list(sorted(targetpath))
        list_images = [self._read_input(path) for path in self.imagepaths]
        list_gt = [self._read_gt(path) for path in self.targetpaths]

        self.images = list_images
        self.targets = list_gt

        self.crop = crop

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index: int):
        input_ID = np.array(self.images[index])
        target_ID = np.array(self.targets[index])

        if self.crop:
            input_ID, target_ID = self.crop_to_papilla(img=input_ID, mask=target_ID)

        if self.transforms:
            transformed = self.transforms(image=input_ID, mask=target_ID)
            input_ID = transformed['image']
            target_ID = transformed['mask']

        if self.torch_transforms:
            input_ID = self.torch_transforms(input_ID)
            target_ID = self.torch_transforms(target_ID)

        # Typecasting
        x = input_ID if torch.is_tensor(input_ID) else self.to_tensor(input_ID)
        y = target_ID if torch.is_tensor(target_ID) else self.to_tensor(target_ID)

        return x, y

    def _read_input(self, path):
        return Image.open(path).convert('RGB')

    def _read_gt(self, path):
        return Image.open(path)

    def crop_to_papilla(self, img, mask, crop=75, radius_start=50, radius_end=250, coordinates=True):
        cropped_img, (cy, cx) = identify_papillae(img, crop=crop, radius_start=radius_start, radius_end=radius_end, coordinates=coordinates)
        cropped_mask = mask[cy[0]-crop:cy[0]+crop, cx[0]-crop:cx[0]+crop]
        return np.asarray(cropped_img), np.asarray(cropped_mask)

    def visualize(self, index):
        origImage, predMask = self[index]
        figure, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 15))

        origImage = np.transpose(origImage.squeeze(0), (1, 2, 0))
        predMask = predMask.squeeze(0)
        print(predMask.shape)
        ax[0].imshow(origImage)
        ax[1].imshow(origImage)
        ax[1].imshow(predMask, cmap=ListedColormap(['#ffffff00', 'white']))
        figure.tight_layout()
        figure.show()

    def visualize_all(self):
        for image, gt in self:
            fig, ax = plt.subplots(nrows=1, ncols=2)
            origImage = np.transpose(image.squeeze(0), (1, 2, 0))
            predMask = gt.squeeze(0)
            ax[0].imshow(origImage)
            ax[1].imshow(origImage)
            ax[1].imshow(predMask, cmap=ListedColormap(['#ffffff00', 'white']))
            fig.tight_layout()
            fig.show()
