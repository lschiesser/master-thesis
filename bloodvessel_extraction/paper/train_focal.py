import torch
from torch.utils.data import DataLoader
from bloodvessel_extraction.drive_loader import DriveDataSet
from util.save_best_model import SaveBestModel, save_model
from util.utils import get_filenames_of_path
import argparse
import segmentation_models_pytorch as smp
import random
import albumentations as A
import matplotlib.pyplot as plt
import os
import json

parser = argparse.ArgumentParser(description="Train Bloodvessel Extractor")
parser.add_argument('-e', '--epochs', type=int, help="Number of epochs", default=10)

args = parser.parse_args()

BATCH_SIZE = 1
LEARNING_RATE = 1e-3
EPOCHS = 40
random_seed = 425
training_size = 0.8

torch.manual_seed(random_seed)
random.seed(random_seed)

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

print("Focal loss")

ROOT_DIR = os.path.abspath('./datasets/DRIVECrop/')
train_images = get_filenames_of_path(ROOT_DIR+"/training/training/images/", '*.tif')
train_targets = get_filenames_of_path(ROOT_DIR+"/training/training/1st_manual/", '*.png')

valid_images = get_filenames_of_path(ROOT_DIR+"/validation/validation/images/", '*.tif')
valid_targets = get_filenames_of_path(ROOT_DIR+"/validation/validation/vessel/", '*.png')



#more augmentation
augs = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=1),
    A.Rotate(limit=10, p=1),
])

augs2 = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=1),
])

augs3 = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.Rotate(limit=10, p=1),
])

# augs_torch = T.Compose([
#     T.ToPILImage(),
#     T.FiveCrop((512, 512)),
#     T.Lambda(lambda crops: torch.stack([T.PILToTensor()(crop) for crop in crops]))
# ])

augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])



print("Loading images...")
train1 = DriveDataSet(imagepath=train_images, targetpath=train_targets, transforms=augs)
train2 = DriveDataSet(imagepath=train_images, targetpath=train_targets, transforms=augs_resize)
train3 = DriveDataSet(imagepath=train_images, targetpath=train_targets, transforms=augs2)
train4 = DriveDataSet(imagepath=train_images, targetpath=train_targets, transforms=augs3)

dataset_train = torch.utils.data.ConcatDataset([train1, train2, train3, train4])

dataset_valid = DriveDataSet(imagepath=valid_images, targetpath=valid_targets, transforms=augs_resize)

dataloader_train = DataLoader(dataset_train, batch_size=BATCH_SIZE, shuffle=True)
dataloader_valid = DataLoader(dataset_valid, batch_size=BATCH_SIZE, shuffle=True)


loss_fn = torch.hub.load(
        'adeelh/pytorch-multi-class-focal-loss',
        model='focal_loss',
        # alpha=torch.tensor([3, 12, 3, 4, 1]),
        alpha=torch.tensor([1/0.78345, 1/0.21654]),
        gamma=2,
        device=device,
        reduction='mean',
        force_reload=False
    )

# model = UNet(n_channels=3, n_classes=2).to(device)
model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=2
).to(device)
model = torch.nn.DataParallel(model)
model.train(True)
# print(model)

optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

save_best_model = SaveBestModel()

S = {"train_loss": [], "test_loss": [], "test_iou": [], "test_acc": [], "test_dice": []}

trainSteps = len(dataloader_train)
testSteps = len(dataloader_valid)

for t in range(EPOCHS):
    print(f"\nEpoch {t+1}\n-------------------------------")
    model.train()
    totalTrainLoss = 0
    totalTestLoss = 0
    totalTestAccuracy = 0
    totalTestIOU = 0
    totalTestDice = 0
    counter = 0
    for batch, (X, y) in enumerate(dataloader_train):
        X, y = X.to(device), y.to(device).long()
        optimizer.zero_grad()
        counter += 1
        pred = model(X)
        loss = loss_fn(pred, y)

        totalTrainLoss += loss.item()

        loss.backward()
        optimizer.step()

    with torch.no_grad():
        model.eval()
        for X, y in dataloader_valid:
            X, y = X.to(device), y.to(device).long()
            pred = model(X)
            valid_loss = loss_fn(pred, y)
            pred = torch.argmax(pred, dim=1)
            tp, fp, fn, tn = smp.metrics.get_stats(pred, y.squeeze(0), mode="multiclass", num_classes=2)
            iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
            dice_score = smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro")
            accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro")
            totalTestAccuracy += accuracy.item()
            totalTestIOU += iou_score.item()
            totalTestDice += dice_score.item()
            totalTestLoss += valid_loss.item()
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()
    avg_train_loss = totalTrainLoss / trainSteps
    avg_test_loss = totalTestLoss / testSteps
    avg_test_acc = totalTestAccuracy / testSteps
    avg_test_iou = totalTestIOU / testSteps
    avg_test_dice = totalTestDice / testSteps
    S['test_iou'].append(avg_test_iou)
    S['test_acc'].append(avg_test_acc)
    S['test_dice'].append(avg_test_dice)
    S['train_loss'].append(avg_train_loss)
    S['test_loss'].append(avg_test_loss)
    print(f"EPOCH: {t + 1}/{EPOCHS}")
    print("Train loss: {:.6f}, Validation loss: {:.6f}".format(
        avg_train_loss, avg_test_loss
    ))
    save_best_model(avg_train_loss, t, model, optimizer, loss_fn, 'outputs/bloodvessel-crop/focal/')
save_model(EPOCHS, model, optimizer, loss_fn, 'outputs/bloodvessel-crop/focal/')

plt.style.use("ggplot")
plt.figure()
plt.plot(S['train_loss'], label="Train loss")
plt.plot(S['test_loss'], label="Validation loss")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend(loc="lower left")
plt.savefig('graphs/focal_bloodvessel_train_loss.png')

# Graph for IOU score and accuracy
plt.style.use("ggplot")
plt.figure()
plt.plot(S['test_iou'], label="Validation IOU")
plt.plot(S['test_acc'], label="Validation accuracy")
plt.plot(S['test_dice'], label="Validation dice")
plt.xlabel("Epochs")
plt.ylabel("%")
plt.legend(loc="lower left")
plt.savefig('graphs/focal_bloodvessel_train_metrics.png')

with open("outputs/bloodvessel-crop/focal/metrics.json", "w") as outfile:
    json.dump(S, outfile)

print("Done!")