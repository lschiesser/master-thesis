from collections import OrderedDict

import torch
import torchvision.utils
from torch.utils.data import DataLoader
from bloodvessel_extraction.drive_loader import DriveDataSet
from models.unet_model import UNet
from util.utils import get_filenames_of_path
from sklearn.metrics import roc_auc_score
import segmentation_models_pytorch as smp
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
import albumentations as A

BATCH_SIZE = 1
random_seed = 42

torch.manual_seed(random_seed)

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

images = get_filenames_of_path("datasets/DRIVECrop/test/test/images/", '*.tif')
targets = get_filenames_of_path("datasets/DRIVECrop/test/test/vessel/", '*.png')

augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])

print("Loading images...")
dataset_test = DriveDataSet(imagepath=images, targetpath=targets, transforms=augs_resize)

dataloader_test = DataLoader(dataset_test, batch_size=BATCH_SIZE, shuffle=True)

print("Images loaded")

print("Loading model...")

# model = UNet(n_channels=3, n_classes=2, sigmoid=True)
model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=2
)
new_state_dict = OrderedDict()
checkpoint = torch.load('outputs/bloodvessel-crop/focal/best_model.pth', map_location=torch.device("cpu"))
new_state_dict = OrderedDict()
for k, v in checkpoint.state_dict().items():
    if k.find(".fc.") == -1:
        if k[:7] == 'module.':
            name = k[7:]  # remove module.
        else:
            name = k
        new_state_dict[name] = v
model.load_state_dict(new_state_dict)
model.to(device)
model.eval()

print("Model loaded")

print("Testing...")


def prepare_plot(origImage, predMask):
    figure, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 15))

    origImage = np.transpose(origImage.squeeze(0), (1, 2, 0))
    predMask = predMask.squeeze(0)
    ax[0].imshow(origImage)
    ax[1].imshow(origImage)
    ax[1].imshow(predMask, cmap=ListedColormap(['#ffffff00', 'white']))
    figure.tight_layout()
    figure.show()


total_metrics = {
    'iou_score': 0,
    'accuracy': 0,
    'dice_score': 0,
}

with torch.no_grad():
    counter = 0
    for batch, (X, y) in enumerate(dataloader_test):
        X, y = X.to(device), y.to(device).long()

        counter += 1
        pred = model(X)
        pred = torch.argmax(pred, 1)
        y = y.squeeze(0)
        tp, fp, fn, tn = smp.metrics.get_stats(pred, y, mode="binary", threshold=0.5)
        iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
        dice_score = smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro")
        accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro")
        total_metrics['iou_score'] += iou_score
        total_metrics['dice_score'] += dice_score
        total_metrics['accuracy'] += accuracy

        prepare_plot(X, pred)

n_samples = len(dataloader_test)
print("IOU Score: {:.3f} Dice Score: {:.3f} Accuracy: {:.3f}". format(
    total_metrics['iou_score'] / n_samples, total_metrics['dice_score'] / n_samples, total_metrics['accuracy'] / n_samples
))
print("Done!")
