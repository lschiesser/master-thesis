import torch
from torch.utils.data import DataLoader
from bloodvessel_extraction.drive_loader import DriveDataSet
from util.save_best_model import SaveBestModel, save_model
from util.utils import get_filenames_of_path
import argparse
import segmentation_models_pytorch as smp
import random
import albumentations as A
import torchvision.transforms as T

parser = argparse.ArgumentParser(description="Train Bloodvessel Extractor")
parser.add_argument('-e', '--epochs', type=int, help="Number of epochs", default=10)

args = parser.parse_args()

BATCH_SIZE = 1
LEARNING_RATE = 1e-3
EPOCHS = args.epochs
random_seed = 425
training_size = 0.8

torch.manual_seed(random_seed)
random.seed(random_seed)

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

images = get_filenames_of_path("datasets/DRIVE/training/training/images/", '*.tif')
targets = get_filenames_of_path("datasets/DRIVE/training/training/1st_manual/", '*.png')


#more augmentation
augs = A.Compose([
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0),
    A.Rotate(limit=10),
])

augs2 = A.Compose([
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0),
])

augs3 = A.Compose([
    A.Resize(512, 512),
    A.Rotate(limit=10),
])

augs_torch = T.Compose([
    T.ToPILImage(),
    T.FiveCrop((512, 512)),
    T.Lambda(lambda crops: torch.stack([T.PILToTensor()(crop) for crop in crops])),
    T.Lambda(lambda crops: torch.stack([crop/255 for crop in crops]))
])

print("Loading images...")
train1 = DriveDataSet(imagepath=images, targetpath=targets, transforms=augs, torch_transforms=augs_torch)
train2 = DriveDataSet(imagepath=images, targetpath=targets, torch_transforms=augs_torch)
train3 = DriveDataSet(imagepath=images, targetpath=targets, transforms=augs2, torch_transforms=augs_torch)
train4 = DriveDataSet(imagepath=images, targetpath=targets, transforms=augs3, torch_transforms=augs_torch)

dataset_train = torch.utils.data.ConcatDataset([train1, train2, train3, train4])
dataloader_train = DataLoader(dataset_train, batch_size=BATCH_SIZE, shuffle=True)

#loss_fn = smp.losses.TverskyLoss(alpha=0.7, beta=0.3, mode='multiclass', classes=2).to(device)
loss_fn = smp.losses.DiceLoss(mode='multiclass').to(device)
# model = UNet(n_channels=3, n_classes=2).to(device)
model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=2
).to(device)
model = torch.nn.DataParallel(model)
model.train(True)
# print(model)

optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

save_best_model = SaveBestModel()

for t in range(EPOCHS):
    print(f"\nEpoch {t+1}\n-------------------------------")
    size = len(dataloader_train.dataset)
    train_running_loss = 0.0
    counter = 0
    for batch, (X, y) in enumerate(dataloader_train):
        X, y = X.to(device), y.to(device).long()
        optimizer.zero_grad()
        counter += 1
        bs, ncrops, c, h, w = X.size()
        X = X.view(-1, c, h, w).float()
        pred = model(X)
        loss = loss_fn(pred, y.squeeze(0).long())
        # pred = torch.argmax(pred, 1)
        # loss = Variable(loss, requires_grad=True)

        train_running_loss += loss.item()

        loss.backward()
        optimizer.step()

        if batch % 10 == 0:
            loss, current = loss.item(), batch * len(X)
            pred = torch.argmax(pred, 1)
            tp, fp, fn, tn = smp.metrics.get_stats(pred, y.squeeze(), mode='multiclass', num_classes=2)
            iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
            accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro")
            print(f"loss: {loss:>7f} IOU: {iou_score:>7f} acc: {accuracy:>7f} [{current:>5d}/{size:>5d}]")
    train_epoch_loss = train_running_loss / counter
    save_best_model(train_epoch_loss, t, model, optimizer, loss_fn, 'outputs/bloodvessel5/')
save_model(EPOCHS, model, optimizer, loss_fn, 'outputs/bloodvessel5/')
print("Done!")