import cv2
import numpy as np
import torch
from skimage.measure import regionprops


def compute_cup_disk_ratio(segmentation):
    if torch.is_tensor(segmentation):
        segmentation = segmentation.squeeze()
        segmentation = segmentation.cpu().detach().numpy()
    # get contour for disk
    contour = segmentation == 0
    # compute vertical diameter of disk
    p = regionprops(contour.astype(np.uint8))
    min_row, min_col, max_row, max_col = p[0].bbox
    disk = max(max_row - min_row, max_col - min_col)
    # get contour for cup
    contour = segmentation == 1
    # compute vertical diameter of cup
    p = regionprops(contour.astype(np.uint8))
    cup = 0
    if len(p) > 0:
        min_row, min_col, max_row, max_col = p[0].bbox
        cup = max(max_row - min_row, max_col - min_col)
    return cup/disk

# add rim-to-disc ratio?
def compute_rim_disc_ratio(segmentation):
    if torch.is_tensor(segmentation):
        segmentation = segmentation.squeeze()
        segmentation = segmentation.cpu().detach().numpy()
    # get contour for disk
    contour = segmentation < 2
    # compute disk area
    p = regionprops(contour.astype(np.uint8))
    min_row, min_col, max_row, max_col = p[0].bbox
    disk = max(max_row - min_row, max_col - min_col)

    # get contour for rim
    contour = segmentation == 0
    rim = np.count_nonzero(contour[256, :] == True)
    return rim/disk

def compute_rim_disc_ratio2(segmentation):
    if torch.is_tensor(segmentation):
        segmentation = segmentation.squeeze()
        segmentation = segmentation.cpu().detach().numpy()
    # get contour for disk
    contour = segmentation < 2
    # compute disk area
    p = regionprops(contour.astype(np.uint8))
    disk = p[0].area

    # get contour for rim
    contour = segmentation == 0
    p = regionprops(contour.astype(np.uint8))
    rim = 0
    if len(p) > 0:
        rim = p[0].area
    return rim/disk




def generate_isnt_masks(size):
    width, height = size[0], size[1]
    temp1 = np.triu(np.ones((width, height), dtype=np.int8))
    temp2 = np.fliplr(temp1)
    mask = temp1 + temp2
    mask[mask > 1] = 1
    mask = np.invert(mask.astype(bool))
    masks = {'inferior': mask}
    quadrants = ["nasal", "superior", "temporal"]
    for quadrant in quadrants:
        mask = np.rot90(mask)
        masks[quadrant] = mask
    return masks
def compute_isnt(segmentation, side="OD"):
    if torch.is_tensor(segmentation):
        segmentation = segmentation.squeeze()
        segmentation = segmentation.cpu().detach().numpy()
    masks = generate_isnt_masks(segmentation.shape)
    regions = ['inferior', 'superior', 'nasal', 'temporal']
    areas = {}
    for quadrant, mask in masks.items():
        # crop to quadrant
        tmp = np.where(mask, segmentation, 3)

        tmp = tmp == 0
        p = regionprops(tmp.astype(np.uint8))
        areas[quadrant] = p[0].area
    # check ISNT rule and return boolean or return the four values?
    # sorted_areas = [(key, areas[key]) for key in regions if key in areas]
    list_sorted_areas = [areas[key] for key in regions if key in areas]
    return list_sorted_areas


def non_increasing(L):
    return all(x >= y for x, y in zip(L, L[1:]))


def compute_bloodvessels_isnt(segmentation, blood_vessels, side="OD"):
    segmentation = segmentation.cpu().detach().numpy()
    masks = generate_isnt_masks(segmentation.shape)
    # identify blood vessels in picture --> sobel filters?


# could apply erosion or skimage.morphology.remove_small_objects to grad to remove some of the white spots