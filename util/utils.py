from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
import glob
import torch
import os
import shutil
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from tqdm import tqdm
from collections import OrderedDict
from occular_metrics.metrics import compute_cup_disk_ratio, compute_isnt

def get_filenames_of_path(path, ext: str = '*'):
    return glob.glob(path + ext)

def split_data(train_split=0.8, test_split=0.2):
    normal = get_filenames_of_path('datasets/SALUS/normal/images/', '*.png')
    glaukom = get_filenames_of_path('datasets/SALUS/glaukom/images/', '*.png')

    normal_train, normal_test = train_test_split(normal, test_size=test_split, random_state=42)
    normal_train_masks, normal_test_masks = [file.replace('images', 'masks') for file in normal_train], [file.replace('images', 'masks') for file in normal_test]
    glaukom_train, glaukom_test = train_test_split(glaukom, test_size=test_split, random_state=42)
    glaukom_train_masks, glaukom_test_masks = [file.replace('images', 'masks') for file in glaukom_train], [file.replace('images', 'masks') for file in glaukom_test]

    if not os.path.exists('datasets/SALUS/train/glaukom/images/'):
        os.mkdir('datasets/SALUS/train/glaukom/images/')

    if not os.path.exists('datasets/SALUS/train/glaukom/masks/'):
        os.mkdir('datasets/SALUS/train/glaukom/masks/')

    if not os.path.exists('datasets/SALUS/train/normal/images/'):
        os.mkdir('datasets/SALUS/train/normal/images/')

    if not os.path.exists('datasets/SALUS/train/normal/masks/'):
        os.mkdir('datasets/SALUS/train/normal/masks/')

    if not os.path.exists('datasets/SALUS/test/glaukom/images/'):
        os.mkdir('datasets/SALUS/test/glaukom/images/')

    if not os.path.exists('datasets/SALUS/test/glaukom/masks/'):
        os.mkdir('datasets/SALUS/test/glaukom/masks/')

    if not os.path.exists('datasets/SALUS/test/normal/images/'):
        os.mkdir('datasets/SALUS/test/normal/images/')

    if not os.path.exists('datasets/SALUS/test/normal/masks/'):
        os.mkdir('datasets/SALUS/test/normal/masks/')

    for file in normal_train:
        shutil.copy(file, 'datasets/SALUS/train/normal/images/')
    for file in normal_train_masks:
        shutil.copy(file, 'datasets/SALUS/train/normal/masks/')
    for file in normal_test:
        shutil.copy(file, 'datasets/SALUS/test/normal/images/')
    for file in normal_test_masks:
        shutil.copy(file, 'datasets/SALUS/test/normal/masks/')

    for file in glaukom_train:
        shutil.copy(file, 'datasets/SALUS/train/glaukom/images/')
    for file in glaukom_train_masks:
        shutil.copy(file, 'datasets/SALUS/train/glaukom/masks/')
    for file in glaukom_test:
        shutil.copy(file, 'datasets/SALUS/test/glaukom/images/')
    for file in glaukom_test_masks:
        shutil.copy(file, 'datasets/SALUS/test/glaukom/masks/')

def prepare_plot(origImage, predMask, targetMask):
    figure, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 15))
    origImage = np.transpose(origImage.squeeze(0), (1, 2, 0))
    targetMask = targetMask.squeeze()
    predMask = predMask.squeeze(0)
    ax[0].imshow(origImage)
    ax[1].imshow(origImage)
    ax[2].imshow(origImage)
    ax[1].imshow(targetMask, cmap=ListedColormap(['#ffffff00', 'blue']))
    ax[2].imshow(predMask, cmap=ListedColormap(['#ffffff00', 'white']))
    figure.tight_layout()
    figure.show()

def calculate_color_ratio(folder):
    folders = glob.glob(folder+"*/*/", recursive=True)
    images = [get_filenames_of_path(folder, '*.png') for folder in folders if "masks" in folder]
    images = [item for sublist in images for item in sublist]
    final_colors = {
        (255, 255, 255): 0,
        (0, 0, 255): 0,
        (0, 0, 0): 0,
        'total': 0
    }
    for image in tqdm(images):
        img = Image.open(image)
        colors = img.getcolors()
        for number, color in colors:
            final_colors[color] += number
        img_size = img.size
        final_colors['total'] += img_size[0] * img_size[1]

    print("Final distirbution:")
    for key in final_colors.keys():
        print(key, ": ", final_colors[key]/final_colors['total'])

def load_model(path):
    checkpoint = torch.load(path, map_location=torch.device("cpu"))
    new_state_dict = OrderedDict()
    for k, v in checkpoint.state_dict().items():
        if k.find(".fc.") == -1:
            if k[:7] == 'module.':
                name = k[7:]  # remove module.
            else:
                name = k
            new_state_dict[name] = v
    return new_state_dict

def compute_metrics(mask):
    cdr = compute_cup_disk_ratio(mask)
    isnt = compute_isnt(mask)
    ophthalmic_metrics = [cdr, [x[1] for x in isnt]]
    ophthalmic_metrics = [item for sublist in ophthalmic_metrics for item in sublist]
    return ophthalmic_metrics

