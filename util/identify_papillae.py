import numpy as np
from PIL import Image
from skimage.transform import hough_circle, hough_circle_peaks
from skimage.feature import canny
from skimage.draw import circle_perimeter, rectangle_perimeter
from skimage.util import img_as_ubyte
from skimage import color

def identify_papillae(img, radius_start=100, radius_end=300, crop=256, coordinates=False):
    col_img = np.asarray(img)
    img = col_img[:, :, 1]  # green channel of picture
    img = img_as_ubyte(img)
    edges = canny(img, sigma=3, low_threshold=10, high_threshold=50)

    hough_radii = np.arange(radius_start, radius_end, 10)
    hough_res = hough_circle(edges, hough_radii)

    accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                               total_num_peaks=1)

    # Draw them
    # start = (cy[0]-256, cx[0]-256)
    # end = (cy[0]+256, cx[0]+256)
    len_y, len_x, _ = col_img.shape
    ys, ye, xs, xe = cy[0] - crop, cy[0] + crop, cx[0] - crop, cx[0] + crop
    if xs < 0:
        xs = 0
    if xe > len_x:
        xe = len_x
    if ys < 0:
        ys = 0
    if ye > len_y:
        ye = len_y
    # circy, circx = rectangle_perimeter(start, end, shape=image.shape)
    col_img = col_img[ys:ye, xs:xe, :]
    if coordinates:
        return Image.fromarray(col_img), (cy, cx)
    return Image.fromarray(col_img)

