# ==============================================================================#
#  Description: Maps colors on RGB-Bitmaps to class indices and return a mask   #
#  containing numeric indices for each pixel instead of RGB-codes.              #
# ==============================================================================#

import torch

# Color codes
import matplotlib.pyplot as plt

colors = [(255, 255, 255),  # disc (0)
          (0, 0, 255),  # cup (1)
          #(0, 255, 0),  # bayonettephänomen (2)
          #(0, 0, 255),  # blutung (3)
          (0, 0, 0)]  # background (4) oder (2)


def map_rgb_to_indices(mask_img):
    width, height = mask_img.shape[0], mask_img.shape[1]
    #print(width, height)
    mapping = {tuple(c): t for c, t in zip(colors, range(len(colors)))}
    #mapping = {(255, 255, 255): 0, (255, 0, 0): 1, (0, 255, 0): 2, (0, 0, 255): 3, (0, 0, 0): 4}

    mask = torch.empty(height, width, dtype=torch.long)
    target = mask_img
    target = torch.from_numpy(target)
    # plt.imshow(target)
    # plt.pause(1)
    #print(target.shape)
    target = target.permute(2, 0, 1)

    for k in mapping:
        torch.set_printoptions(profile="short")
        idx = (target == torch.tensor(k, dtype=torch.uint8).unsqueeze(1).unsqueeze(2))
        validx = (idx.sum(0) == 3)
        mask[validx] = torch.tensor(mapping[k], dtype=torch.long)

    return mask