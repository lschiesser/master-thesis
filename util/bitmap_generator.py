# ==============================================================================#
#  Description: Extracts bitmaps from CVATs XML-Polygons and writes them        #
#  from to specified destination                                                #
#  Use:  python bitmap_generator.py --image-dir [IMG_DIR] --cvat-xml [CVAT.XML] #
#  --output-dir [OUTPUT]                                                        #
# ==============================================================================#

import argparse
import os

import cv2
import numpy as np
from lxml import etree
from tqdm import tqdm


def color_to_shape(shape):
    type = shape['type'].lower()
    return {
        'disc': (255, 255, 255),  # white
        'cup': (255, 0, 0),  # blue
        'bayonettphänomen': (0, 255, 0),  # green
        'blutung': (0, 0, 255),  # red
     }.get(type, (0, 0, 0))


def parse_args():
    parser = argparse.ArgumentParser(
        fromfile_prefix_chars='@',
        description='Convert CVAT XML annotations to contours')

    parser.add_argument(
        '--image-dir', metavar='DIRECTORY', required=True,
        help='directory with input images'
    )
    parser.add_argument(
        '--cvat-xml', metavar='FILE', required=True,
        help='input file with CVAT annotation in xml format'
    )
    parser.add_argument(
        '--output-dir', metavar='DIRECTORY', required=True,
        help='directory for output masks'
    )
    parser.add_argument(
        '--scale-factor', type=float, default=1.0,
        help='choose scale factor for images'
    )
    return parser.parse_args()


def parse_anno_file(cvat_xml, image_name):
    root = etree.parse(cvat_xml).getroot()
    anno = []
    image_name_attr = ".//image[@name='{}']".format(image_name)
    for image_tag in root.iterfind(image_name_attr):
        image = {}
        for key, value in image_tag.items():
            image[key] = value
        image['shapes'] = []
        for poly_tag in image_tag.iter('polygon'):
            polygon = {'type': poly_tag.get('label')}
            for key, value in poly_tag.items():
                polygon[key] = value
            image['shapes'].append(polygon)
        image['shapes'].sort(key=lambda x: int(x.get('z_order', 0)))
        anno.append(image)
    return anno


def create_mask_file(width, height, bitness, background, shapes, scale_factor):
    mask = np.full((height, width, bitness // 8), background, dtype=np.uint8)
    # sort shapes in reverse alphabetical order this way disc will be painted first, then cup, then blood, bayonette
    shapes = sorted(shapes, key=lambda d: d['label'], reverse=True)
    for shape in shapes:
        points = [tuple(map(float, p.split(','))) for p in shape['points'].split(';')]
        points = np.array([(int(p[0]), int(p[1])) for p in points])
        points = points * scale_factor
        points = points.astype(int)
        color = color_to_shape(shape)
        mask = cv2.drawContours(mask, [points], -1, color=color, thickness=0)
        mask = cv2.fillPoly(mask, [points], color=color)
    return mask


def main():
    args = parse_args()
    img_list = [f for f in os.listdir(args.image_dir) if os.path.isfile(os.path.join(args.image_dir, f))]
    mask_bitness = 24
    for img in tqdm(img_list, desc='Writing contours:'):
        img_path = os.path.join(args.image_dir, img)
        anno = parse_anno_file(args.cvat_xml, img)
        background = []
        is_first_image = True
        for image in anno:
            if is_first_image:
                current_image = cv2.imread(img_path)
                _height, _width, _ = current_image.shape
                background = np.zeros((_height, _width, 3), np.uint8)
            is_first_image = False
            output_path = os.path.join(args.output_dir, img.split('.')[0] + '.png')
            background = create_mask_file(_width,
                                          _height,
                                          mask_bitness,
                                          background,
                                          image['shapes'],
                                          args.scale_factor)
            cv2.imwrite(output_path, background)


if __name__ == "__main__":
    main()