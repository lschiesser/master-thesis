import torch
from tqdm import tqdm
from PIL import Image
import numpy as np
from models.embedding import ResNetEmbedding
import torchvision.transforms as T
from util.utils import get_filenames_of_path


def extract_resnet_features(input_folder, function=None, nr_channels=3):
    file_list = get_filenames_of_path(input_folder, '*.png')
    device = "cuda" if torch.cuda.is_available() else "cpu"
    torch.manual_seed(42)
    resnet = ResNetEmbedding(nr_channels).to(device)
    resnet.eval()

    for i in tqdm(range(len(file_list))):
        file = file_list[i]
        image = np.array(Image.open(file))

        if function:
            image = function(image)
        else:
            image = T.ToTensor()(image)
        image = image.unsqueeze(0)
        image = image.to(device)
        output = resnet(image)

        output_path = file.replace("AugMini", "EmbedSALUS").replace(".png", ".pt")

        torch.save(output, output_path)
