from collections import OrderedDict
from PIL import Image
import albumentations as A
import torchvision.transforms as T
import torch
import segmentation_models_pytorch as smp
import imageio
import os
import numpy as np



def load_model(device):
    model = smp.Unet(
        encoder_name='resnet34',
        encoder_weights='imagenet',
        decoder_use_batchnorm=True,
        classes=2
    )
    checkpoint = torch.load('outputs/bloodvessel-crop/focal/best_model.pth', map_location=torch.device("cpu"))
    new_state_dict = OrderedDict()
    for k, v in checkpoint.state_dict().items():
        if k.find(".fc.") == -1:
            if k[:7] == 'module.':
                name = k[7:]  # remove module.
            else:
                name = k
            new_state_dict[name] = v
    model.load_state_dict(new_state_dict)
    model.to(device)
    model.eval()
    return model


def preaugment(images, output_path, augment=True):
    device = "cuda" if torch.cuda.is_available() else "cpu"
    blood_vessel_model = load_model(device)
    to_tensor = T.ToTensor()

    augs = A.Compose([
        A.CLAHE(p=1),
        A.Resize(512, 512),
        A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=1),
        A.Rotate(limit=10, p=1),
    ])

    augs2 = A.Compose([
        A.CLAHE(p=1),
        A.Resize(512, 512),
        A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=1),
    ])

    augs_resize = A.Compose([
        A.CLAHE(p=1),
        A.Resize(512, 512)
    ])

    for image_path in images:
        filename = os.path.split(image_path)[-1]
        image, mask = np.array(Image.open(image_path).convert('RGB').resize((512, 512))), np.array(Image.open(image_path.replace("images", "masks")).resize((512, 512)))
        x_temp = augs_resize(image=image)["image"]
        x_temp = to_tensor(image).unsqueeze(0).to(device)
        vessels = blood_vessel_model(x_temp)
        vessels = torch.argmax(vessels, 1)
        vessels = vessels.squeeze(0)
        vessels = vessels.cpu().detach().numpy()

        glaukom = "normal"
        if "glaukom" in image_path:
            glaukom = "glaukom"
        if augment:
            deformed_1 = augs(image=image, masks=[mask, vessels])
            imageio.imsave(os.path.join(output_path, "images", glaukom +"_deformed_1_" + filename.rstrip()), deformed_1['image'])
            imageio.imsave(os.path.join(output_path, "biomarkers", glaukom +"_deformed_1_" + filename.rstrip()), deformed_1['masks'][0])
            imageio.imsave(os.path.join(output_path, "vessels", glaukom +"_deformed_1_" + filename.rstrip()), deformed_1['masks'][1])

            deformed_2 = augs2(image=image, masks=[mask, vessels])
            imageio.imsave(os.path.join(output_path, "images", glaukom +"_deformed_2_" + filename.rstrip()), deformed_2['image'])
            imageio.imsave(os.path.join(output_path, "biomarkers", glaukom +"_deformed_2_" + filename.rstrip()), deformed_2['masks'][0])
            imageio.imsave(os.path.join(output_path, "vessels", glaukom +"_deformed_2_" + filename.rstrip()), deformed_2['masks'][1])

        resized = augs_resize(image=image, masks=[mask, vessels])
        imageio.imsave(os.path.join(output_path, "images", glaukom +"_resized_" + filename.rstrip()), resized['image'])
        imageio.imsave(os.path.join(output_path, "biomarkers", glaukom +"_resized_" + filename.rstrip()), resized['masks'][0])
        imageio.imsave(os.path.join(output_path, "vessels", glaukom +"_resized_" + filename.rstrip()), resized['masks'][1])
