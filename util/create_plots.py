from collections import OrderedDict

import torch
import torchvision.utils
from torch.utils.data import DataLoader
from data_loaders.image_loader import SegmentationDataSet
from models.unet_model import UNet
from util.utils import get_filenames_of_path
from sklearn.metrics import roc_auc_score
import segmentation_models_pytorch as smp
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
import albumentations as A

BATCH_SIZE = 1
random_seed = 42

torch.manual_seed(random_seed)

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

# TODO load in data
images = get_filenames_of_path("datasets/SALUS/test/normal/images/", '*.png') + get_filenames_of_path("datasets/SALUS/test/glaukom/images/", '*.png')
targets = get_filenames_of_path("datasets/SALUS/test/normal/masks/", '*.png') + get_filenames_of_path("datasets/SALUS/test/glaukom/masks/", '*.png')


augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])

print("Loading images...")
# create datasets
dataset_test = SegmentationDataSet(images=images, targets=targets, transforms=augs_resize)

dataloader_test = DataLoader(dataset_test, batch_size=BATCH_SIZE, shuffle=True)

print("Images loaded")

print("Loading model...")

# TODO load model
model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=3
)
new_state_dict = OrderedDict()
checkpoint = torch.load('outputs/segmentation/tversky/best_model.pth', map_location=torch.device("cpu"))
new_state_dict = OrderedDict()
for k, v in checkpoint.state_dict().items():
    if k.find(".fc.") == -1:
        if k[:7] == 'module.':
            name = k[7:]  # remove module.
        else:
            name = k
        new_state_dict[name] = v
model.load_state_dict(new_state_dict)
model.to(device)
model.eval()

print("Model loaded")

print("Testing...")


def save_images(origImage, predMask, gt, counter):
    figure, ax = plt.subplots()

    origImage = np.transpose(origImage.squeeze(0), (1, 2, 0))
    predMask = predMask.squeeze(0)
    gt = gt.squeeze()
    ax.imshow(origImage)
    ax.axis("off")
    figure.savefig("graphs/seg/tversky/st_orig_{}.png".format(counter), bbox_inches="tight")
    plt.close(figure)
    figure, ax = plt.subplots()
    #ax.imshow(origImage)
    ax.imshow(predMask, cmap=ListedColormap(['white', 'blue', 'black']))
    ax.axis("off")
    figure.savefig("graphs/seg/tversky/st_pred_{}.png".format(counter), bbox_inches="tight")
    plt.close(figure)
    figure, ax = plt.subplots()
    #ax.imshow(origImage)
    ax.imshow(gt, cmap=ListedColormap(['white', 'blue', 'black']))
    ax.axis("off")
    figure.savefig("graphs/seg/tversky/st_gt_{}.png".format(counter), bbox_inches="tight")
    plt.close(figure)


total = len(dataloader_test)
with torch.no_grad():
    counter = 0
    for batch, (X, y, _) in enumerate(dataloader_test):
        X, y = X.to(device), y.to(device).long()

        counter += 1
        pred = model(X)
        pred = torch.argmax(pred, 1)
        if counter % 6 == 0:
            print("{}/{}".format(counter, total))
            save_images(X, pred, y, counter)

print("Done!")
