A literature review was carried out to get an overview of current and past 
approaches to the task of optic disc and cup, as well as blood vessel 
segmentation. This chapter describes the process and results of the literature 
review and how they relate to the thesis.
\section{Literature Search and Evaluation}
The search engine Pubmed was used for this literature review, which 
``comprises more than 35 million citations for biomedical literature from 
MEDLINE, life science journals, and online books'' 
\cite{noauthor_pubmed_nodate}. The search was carried out using the query 
\textit{(Glaucoma) AND (segmentation) AND (fundus)} and 
resulted in 387 entries. Since only one database was used for this review, the 
search query was designed to be as broad as possible. This way, more potential 
publications were obtained. The papers were screened and excluded 
according to the following exclusion criteria:
\begin{itemize}
 \item The publication did not cover automated diagnosis of glaucoma either 
through segmentation, classification, or a combination of both
 \item The publication did not cover human diagnosis
 \item The publication was not available in English or German
 \item The publication could not be retrieved due to, e.g., retraction, 
licensing issues, or other reasons
 \item The publication was published before 2010
\end{itemize}
Table \ref{tab:exlcusion-numbers} details the number of excluded publications 
and why they were excluded. The final review included 114 papers. For each of 
these papers, a thorough screening process was conducted to extract the 
relevant information, including the research question, methods, results, and 
important points from the discussion. When introducing the different approaches 
to the task of glaucoma diagnosis, relevant studies from the final review will 
be mentioned as appropriate.
\begin{table}[ht]
\centering
\begin{tabularx}{\textwidth}{X|c}
Exclusion criterion                                    & 
Number of publications \\ \hline
Not about automated diagnosis of glaucoma via segmentation or classification & 
227                    \\
Not about human diagnosis                                                    & 3 
                     \\
Not in English or German                                                     & 7 
                     \\
Could not be retrieved                                                       & 
7                     \\
Published before 2010 & 4 \\ \hline \hline
Total number of excluded publications & 248 \\ \hline
\end{tabularx}
\caption{Exclusion criteria and number of excluded papers for each criterion}
\label{tab:exlcusion-numbers}
\end{table}

\section{Segmentation and Classification in Glaucoma Diagnosis}
In general, there are various approaches to \textit{segmentation} and 
\textit{classification} in 
glaucoma diagnosis. They can be categorized into two broad categories: 
computer vision and deep learning based approaches. Sometimes this 
differentiation is not entirely clear since many studies combine both 
approaches, 
primarily segmenting biomarkers in medical images using computer vision 
techniques and then classifying the created feature vectors using machine or 
deep learning techniques. This section provides an overview 
of the methods used in both approaches.
\subsection{Computer Vision Based Approaches}
\paragraph{Techniques based on image and biomarker characteristics.}
Many computer vision based methods use an image's or biomarker's
 characteristics, e.g., illumination, color, or texture. In particular, the 
segmentation or identification of the optic disc is often based on the 
assumption that it is the brightest spot in the image \cite{veena_review_2020}. 
\citeauthor{pachiyappan_automated_2012} \cite{pachiyappan_automated_2012} use 
low pass filtering\footnote{A 
low-pass filter selectively allows signals with frequencies below a specific 
cutoff frequency to pass through while reducing the amplitude of signals with 
frequencies above the cutoff frequency.} and thresholding\footnote{Thresholding 
is a process used in computer vision to create a binary 
image by converting grayscale or color images into black and white images based 
on a specific pixel intensity threshold. While for simple thresholding, the 
threshold value is global, i.e., applying to every single pixel equally, in 
adaptive thresholding, the threshold only applies to smaller regions in the 
image.} 
to segment the optic disc based on this assumption 
\cite{pachiyappan_automated_2012}. \citeauthor{akram_glaucoma_2015} 
\cite{akram_glaucoma_2015} used 
Laplacian of Gaussian filters \footnote{Laplacian of Gaussian filters are a type 
of edge detection filter that combine a Gaussian smoothing with a Laplacian 
operator. They can be applied in one step by convolving an image with a 
Laplacian of Gaussian kernel \cite{nixon_feature_2020}.} and 2-D Gabor 
wavelets\footnote{D-Gabor wavelets are mathematical functions used to analyze 
image textures and orientations. They can be viewed as multiscale 
partial differential operators of a given order \cite{nixon_feature_2020}.} to 
enhance high intensity 
(i.e., bright) regions and the vascular pattern, respectively. The optical disc 
was located using the assumption that it is the brightest spot in the image and 
by identifying the area of highest vascular concentration since the major 
retinal blood vessels converge onto the optic disc. The next step was to segment 
the optic disc from the region of interest (ROI) image using morphological 
closing\footnote{Morphological closing is a common image processing technique. 
Closing can effectively fill small holes using dilation and subsequent erosion 
while maintaining the shape and size of larger objects in an image 
\cite{nixon_feature_2020}.} on 
the red channel. Subsequently, adaptive thresholding was applied to create a 
binary image. To extract the optic cup, the green channel of the ROI image was 
used, and adaptive thresholding was applied \cite{akram_glaucoma_2015}. The red 
channel is often used to segment the optic disc because it provides a higher 
contrast between the optic disc and the background than the other channels. 
Similar reasoning can be applied to the green channel and optic cup 
segmentation \cite{oktoeberza_optic_2015, akram_glaucoma_2015}.
\\
Another frequent assumption is that the optic disc and cup are roughly 
circular 
or elliptical. Based on this assumption 
\citeauthor{fuente-arriaga_application_2014} 
\cite{fuente-arriaga_application_2014} use the circular Hough 
transformation\footnote{The Hough transformation is a computer vision 
technique that detects geometric shapes, such as lines or circles, in an 
image \cite{nixon_feature_2020}.} to initially segment the optic disc and 
further improve the 
segmentation using active contour models 
\cite{fuente-arriaga_application_2014}. This approach is also used by 
\citeauthor{jun_cheng_self-assessment_2013} 
\cite{jun_cheng_self-assessment_2013}. Circular or elliptical Hough transforms 
are usually used to generate an initial contour of the optic disc or cup. Then 
other methods are used to improve the segmentation, e.g., active contour 
modeling \cite{sekhar_automated_2011, sun_optic_2015}.
\paragraph{Active contour modeling.}
Active contour modeling is an image segmentation method. It starts with an 
initial contour represented by a set of points which can be either given 
manually or by other image processing techniques. It then improves 
the position of the points iteratively according to an energy function that 
encodes desired 
properties of the final, desired contour. The energy function is a mathematical 
function that describes the shape of a contour which is a curve that 
represents the boundary of an object or region of interest in an image
\cite{kass_snakes_1988}.
\\
\citeauthor{zhou_optic_2019} \cite{zhou_optic_2019} used a locally statistical 
active contour model 
to segment the optic disc and cup. After cropping to the papillary region of 
the fundus image, they removed the blood vessels in this region using B-COSFIRE 
filters \cite{azzopardi_trainable_2015} and image inpainting 
algorithms\footnote{Inpainting describes a method in computer vision to fill in 
missing or damaged parts of an image based on its surrounding context.} 
\cite{telea_image_2004}. For 
segmentation, only the red channel 
of the fundus images was used. The initial points were obtained using Canny 
edge detectors and circular Hough transform \cite{zhou_optic_2019}. 
\citeauthor{muramatsu_automated_2011} \cite{muramatsu_automated_2011} determined 
the optic disc's outline using 
active contour modeling with initial points from a Canny edge 
detector\footnote{The canny edge detector is a multi-stage algorithm that can 
detect 
edges. It includes smoothing, gradient calculation, non-maximum suppression, 
and thresholding \cite{shapiro_computer_2001}.}. The optic 
cup outline was obtained using a depth map constructed by stereo disparity 
which was then thresholded \cite{muramatsu_automated_2011}. 
\citeauthor{hatanaka_improved_2014} \cite{hatanaka_improved_2014} segmented the 
optic disc based on the red 
channel of a fundus image. The initial points for active contour modeling were 
obtained using a Canny edge detector. After segmenting the optic disc, the 
optic 
cup was obtained using zero-crossing\footnote{Zero-crossing is a technique in 
computer vision to detect edges. It identifies points where the brightness 
value of adjacent pixels change sign.}. The segmentation of the optic disc and 
cup 
was mainly used to compute the cup-to-disc ratio. Therefore, no evaluation of 
the segmentation was provided \cite{hatanaka_improved_2014}. Chapter 
\ref{sec:cdr} defines the cup-to-disc ratio and explains why it is important 
in glaucoma detection.

\paragraph{Superpixel classification.}
Superpixel classification is another image segmentation technique. The 
algorithm starts over-segmenting the image into many small and 
irregular regions (sometimes even individual pixels). Subsequently, the 
regions are merged into larger, more regular regions based on 
similarity measures such as color, texture, or shape \cite{achanta_slic_2012}.
\\
\citeauthor{cheng_superpixel_2013} \cite{cheng_superpixel_2013} used the simple 
linear interactive clustering (SLIC) algorithm to create superpixels which were 
then classified as optic disc or optic cup separately in binary classification 
tasks using support vector machines (SVM)\footnote{Support Vector Machines are 
a popular machine learning algorithm used for classification and regression 
tasks. They classify data into different classes by identifying the optimal 
decision boundary separating the data \cite{cortes_support-vector_1995}.} where 
the  \cite{cheng_superpixel_2013}. 
\citeauthor{ong_automatic_2020} \cite{ong_automatic_2020} used a similar 
approach to segmenting the optic disc. The optic cup's outline, however, was 
obtained using elliptical Hough transform and subsequent active contour 
modeling \cite{ong_automatic_2020}.
\par
Computer vision based approaches to segmentation and classification are powerful 
tools. They are not only computationally efficient but are also easy to 
implement. Moreover, they only need small amounts of training data as most of 
the features are handcrafted. This is also one of the drawbacks of these 
approaches, as they often require careful tuning of parameters and thresholds 
to achieve accurate segmentation results. This is usually time-consuming and 
requires expert knowledge. Further, these methods are only tuned to a specific 
subset of images where illumination, contrast, and quality are similar, leading 
to a lack of generalizability. Therefore, deep learning based approaches 
have been developed to counter these disadvantages.
\subsection{Deep Learning Based Approaches}
\paragraph{CNN architectures.}
\textit{Convolutional Neural Networks (CNNs)} CNNs are a powerful machine 
learning approach used for computer vision tasks, like image classification and 
object recognition. They are able to learn hierarchical representations of 
images or other input data by performing convolutions, allowing them to capture 
local and global patterns. Convolutional blocks, which typically consist of 
convolutional layers, normalization layers, and activation functions, are used 
to build larger CNN architectures. The design of these blocks and the number of 
layers used can vary depending on the specific task and dataset being used. CNNs 
have been shown to achieve state-of-the-art performance on a wide range of 
computer vision tasks \cite{goodfellow_deep_2016}. In the context of this 
review, classical CNNs are also often used for classification instead of 
segmentation. \citeauthor{diaz-pinto_cnns_2019} 
\cite{diaz-pinto_cnns_2019}
evaluated different fine-tuned pre-trained CNNs regarding their glaucoma 
classification performance. They compared VGG16, VGG19 
\cite{simonyan_very_2015}, InceptionV3 \cite{szegedy_rethinking_2015}, ResNet50 
\cite{he_deep_2015}, 
and Xception \cite{chollet_xception_2017}, as well as how many layers should be 
trained. They found that so-called deep tuning, i.e., training all layers in 
the pre-trained network, 
achieves the best results and that the Xception architecture performed best, 
especially when taking the trade-off between model size and obtained 
performance into account \cite{diaz-pinto_cnns_2019}. Sometimes, multiple CNNs 
are used in 
an ensemble framework to classify fundus images. Ensemble learning is a machine 
learning technique that combines several models to improve the accuracy and 
robustness of predictions. Each model makes an independent prediction which is 
then combined to obtain a final prediction. There are various mechanisms for 
combining individual predictions, such as bagging, voting, and stacking. 
Ensemble 
learning can reduce a model's variance and improve its generalization 
performance, resulting in a higher accuracy and more robust predictions 
\cite{goodfellow_deep_2016}.
\citeauthor{aziz-ur-rehman_ensemble_2021} \cite{aziz-ur-rehman_ensemble_2021} 
assessed different ensemble methods 
with different voting mechanisms. They also provided the individual 
performance of each pre-trained network. The ensemble classifier included 
AlexNet \cite{krizhevsky_imagenet_2012}, InceptionV3, InceptionResNetV2 
\cite{szegedy_inception-v4_2016}, and NasNet-Large \cite{qin_nasnet_2020}. 
NasNet-Large  performed best with an 
accuracy of 99.3\%, 99.4\% specificity, and a sensitivity of 99.1\%. An 
ensemble 
of all four classifiers voting based on accuracy/score based weighted 
averaging\footnote{In accuracy/score based weighted 
averaging (ASWA), every model receives a weight factor based on its accuracy or 
score. ASWA is the sum of all predictions multiplied by their weight factor 
\cite{aziz-ur-rehman_ensemble_2021}.} 
performed even better than NasNet alone, with an accuracy of 99,5\%, a 
sensitivity of 99.1\%, and a specificity of 99.7\% 
\cite{aziz-ur-rehman_ensemble_2021}.
\citeauthor{joshi_glaucoma_2022} \cite{joshi_glaucoma_2022} used majority voting 
and trained an ensemble 
framework with ResNet50, VGG16, and GoogleNet \cite{szegedy_going_2014} and 
evaluated the individual networks' performance. ResNet50 performed best 
individually, with an 
accuracy of 85.56\%, a sensitivity of 77.81\%, and a specificity of 92.06\%. 
The ensemble classifier performed better with an accuracy of 88.96\%, a 
sensitivity of 81.26\%, and a specificity of 92.06\% \cite{joshi_glaucoma_2022}.

\paragraph{U-Net and U-Net based architectures.}
\textit{U-Net} is a well-performing segmentation model that achieves 
competitive 
results with small datasets \cite{ronneberger_u-net_2015}. A more detailed 
description of U-Net is provided 
in Chapter \ref{sec:u-net}, as it is also used in this thesis. 
U-Net is often used in its original form. 
For example, \citeauthor{wang_coarse--fine_2019} \cite{wang_coarse--fine_2019} 
segmented the optic disc in 
fundus color images using the original U-Net architecture. They trained it 
for 300 
epochs using stochastic gradient descent (SGD) with a learning rate of 0.001, 
decreasing by a factor of 10 
every 30 epochs and a momentum of 0.95. They achieved an IoU of 0.874 and a 
Dice Score of 0.925 \cite{wang_coarse--fine_2019}. U-Net is not only used in 
its original form. It is often extended or changed using different 
convolutional blocks or techniques like attention, additional pathways, or 
adversarial networks. \citeauthor{jin_optic_2020} \cite{jin_optic_2020} use 
dense blocks instead of 
residual blocks, similar to DenseNet \cite{huang_densely_2018}, increasing the 
number of 
convolutional layers per block and their skip connections. They additionally 
replaced the residual blocks in the expansive path with so-called aggregation 
channel attention upsampling (ACAU) modules, similar to Squeeze and Excitations 
Blocks\footnote{For more information on the ACAU module and Squeeze and 
Excitation Blocks, please refer to \cite{hu_squeeze-and-excitation_2019}.} 
\cite{jin_optic_2020}.
\citeauthor{martins_offline_2020} switched the residual blocks with depth-wise 
separable convolutional blocks \cite{martins_offline_2020}.
\\
Further, some publications build on the idea of U-Net and extend it. 
\citeauthor{fu_joint_2018} \cite{fu_joint_2018} developed M-Net, a U-Net 
inspired network that 
segments optic disc and cup jointly using a multiscale input with U-Net as the 
main processing body and employs a side-output layer. The multiscale input 
layer creates an image pyramid\footnote{An image pyramid is a 
multiscale representation of an image often used in image processing 
and computer vision \cite{shapiro_computer_2001}.}. The side-output layer 
creates early (local)
classification maps for different scale layers which are in the end aggregated 
into a final classification map. Interestingly, they convert their input image 
after cropping to the papillary region to polar coordinates. They argue that 
bias can be introduced due to a great imbalance between cup and disc proportion 
which leads to overfitting. 
Therefore, their model is trained on polar coordinate 
images of the papillary region and accordingly generates segmentation maps 
which 
have to be converted back to cartesian coordinates \cite{fu_joint_2018}. 
\citeauthor{yuan_multi-scale_2021} \cite{yuan_multi-scale_2021} developed 
W-Net, 
a 
network that cascades two U-Net networks. The two networks are linked 
through connections between the layers with the same scale. 
They also use a multiscale input and output 
strategy to improve the network's segmentation 
process. They also convert their images to polar coordinates 
\cite{yuan_multi-scale_2021}.

\paragraph{GAN architectures.}
Generative Adversarial Networks (GANs) are a type of deep learning architecture 
that can be used for various image processing tasks, including image 
classification and segmentation. 
GANs consist of two neural networks - a generator and a discriminator. 
The generator generates new images while the discriminator 
tries to determine whether a given image is real or fake. Both networks are 
trained simultaneously in a competitive process where the generator tries 
to generate images that can fool the discriminator and where the discriminator 
tries to distinguish correctly between real and fake 
images \cite{goodfellow_generative_2014}. After training, the generator can be 
used to 
augment and extend datasets because it has learned to generate realistic 
images. Similarly, the discriminator can then be used as a segmentation model 
\cite{xun_generative_2022}.
\\
\citeauthor{liu_joint_2019} \cite{liu_joint_2019} trained a generative 
adversarial structure 
consisting of a segmentation net, a discriminator, and a generator to segment 
the optic disc and cup. Labeled data in the form of fundus images and 
corresponding ground truth segmentation masks are used to train the 
architecture. 
The segmentation net creates a segmentation map from the fundus image, while 
the generator creates a fundus image from the ground truth segmentation mask. 
The discriminator gets the labeled samples, the generated segmentation, and 
the fundus image as input and should discriminate between real and fake images. 
The architecture achieves competitive results with a mean IoU of 
0.8655 \cite{liu_joint_2019}. 
\citeauthor{kadambi_wgan_2020} \cite{kadambi_wgan_2020} employed a different 
version of GANs, namely a Wasserstein generative network (WGAN). WGANs use the 
Wasserstein distance as a measure of domain discrepancy. The use of the 
Wasserstein discrepancy measure proved to generate more stable gradients and 
improved performance compared with existing domain adaptation methods 
\cite{kadambi_wgan_2020}.

\section{Summary}
In summary, there are many different approaches to the segmentation of 
biomarkers and the classification of glaucoma. The most used biomarkers during 
segmentation are the optic disc, cup and blood vessels. Although no 
studies using only blood vessels for glaucoma classification were found 
during the literature search. Notably, many 
studies did not use the entire fundus image but instead identified the 
papillary region as a region of interest. The images used for segmentation and 
classification were often cropped to this region. Cropping fundus 
images to the papillary region is a sensible approach as this area contains 
crucial biomarkers that are indicative of various ocular and systemic diseases 
\cite{mayya_empirical_2023, greslechner_klinische_2016}.
\\
The approaches shown throughout this chapter can be categorized into computer 
vision and deep learning based approaches. A major shortcoming of computer 
vision based approaches is that they rely on handcrafted features. 
Additionally, these methods can lack generalizability since they are dependent 
on illumination, contrast, and a high degree of similarity. Further, they often 
need to remove disruptive factors like blood vessels from the image since blood 
vessels cause sudden changes in intensity when crossing the cup-disc boundary.
Therefore, deep learning methods might be advantageous since they can learn 
features from fundus images of varying quality and illumination. In case of 
deep learning, these systems often use only one input type, namely the images, 
to make their predictions. Especially in classification, only 
unsegmented fundus images or segmentation maps are used to make a 
prediction. However, researchers have rarely employed  segmentation maps 
combined with the fundus image to make a 
classification. Moreover, ophthalmic metrics 
like the cup-to-disc ratio are rarely used in combination with 
imaging. This lack of considering combinatorial approaches leaves a gap in the 
literature this thesis tries to fill. \\
The proposed multi-step framework not only segments the fundus images but also 
uses the segmentations in combination with the original fundus image and 
ophthalmic 
metrics that are computed based on the segmentations to classify glaucoma. It 
also investigates whether this inclusion of additional information benefits the 
classification process compared to a classifier using only 
fundus images.
