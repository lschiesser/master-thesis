\textit{Glaucoma} is a group of eye diseases mainly characterized by the 
progressive 
degradation of retinal ganglion cells \cite{weinreb_pathophysiology_2014}. This 
degradation leads to cupping, a characteristic appearance of the \textit{optic 
disc}, and progressive visual field defects leading to vision loss 
\cite{weinreb_pathophysiology_2014, greslechner_klinische_2016}. Glaucoma is 
the leading cause of irreversible blindness and the second leading cause of 
blindness worldwide \cite{weinreb_pathophysiology_2014}. The leading cause 
of blindness worldwide is cataracts, a cloudiness of the optic lens, which can 
be reversed by lens replacement surgery.  Glaucoma affects more than 70 
million people worldwide, of which 10\% are bilaterally blind 
\cite{allison_epidemiology_2020}. Glaucoma's prevalence is expected to grow to 
over 111 million people in 2040 \cite{tham_global_2014, 
allison_epidemiology_2020}. \\
The causes of glaucoma as well as the factors contributing to its progression 
have yet to be fully understood. 
Nevertheless, a \textit{high intraocular pressure} (IOP) is one of the most 
important factors for the development of glaucoma. An elevated IOP is 
hypothesized to lead to 
ganglion cell death due to mechanical stress and strain on the posterior 
structures of the eye where the \textit{optic nerve head} formed by the retinal 
ganglion cells is located \cite{allison_epidemiology_2020, 
weinreb_pathophysiology_2014}. However, IOP is not a reliable indicator of 
glaucoma since the disease can also appear in patients with normal IOP and 
individuals with high IOP may never develop glaucoma 
\cite{weinreb_pathophysiology_2014}. Therefore, other risk factors are also 
considered during diagnosis including (older) age, (female) gender, 
hypotension, and hypertension \cite{allison_epidemiology_2020}.
\par
Initially, glaucoma progresses asymptomatically. Symptoms only present 
themselves with substantial neural damage. They manifest mainly in vision loss 
through visual field degradation leading to a concomitant reduction in quality 
of life and the ability to perform daily tasks (e.g., reading, driving, face 
recognition) \cite{allison_epidemiology_2020, weinreb_pathophysiology_2014}. 
Therefore, early diagnosis and treatment are crucial to preserve the quality of 
life of affected individuals. Moreover, with an aging population, more 
individuals will require screening for this disease \cite{tham_global_2014}. As 
healthcare systems are already overwhelmed, there is an increasing need for 
automated screening tools to support medical professionals like 
ophthalmologists with this task. 
% reword?
Deep learning based methods have the potential to support clinicians in their 
efforts to provide early treatment for glaucoma. These methods use complex 
neural networks that can learn from large amounts of data, including fundus 
images. By analyzing these images, deep learning algorithms can accurately 
detect the biomarkers associated with glaucoma, even in its early stages. This 
can provide clinicians with valuable information to support their diagnosis and 
treatment decisions.
\par
The SALUS study is a two-arm, multicenter, randomized clinical trial evaluating 
the healthcare situation of glaucoma patients in Germany and aims to improve 
it. To detect glaucoma in the \textit{fundus images}, a multi-step framework 
was developed as part of this study. The framework first segments specific 
biomarkers and then utilizes the segmentation results along with the fundus 
images to detect glaucoma. A detailed description of the framework is provided 
in Chapter \ref{sec:implementation}.
\par
In fundus images, glaucoma manifests in characteristic changes of the optic 
nerve head. The appearance and size of 
the \textit{optic disc and cup} and their ratio are often considered. In 
particular, a large optic cup that covers the optic disc substantially is a 
common indicator of glaucoma. Further, 
ophthalmologists check for changes in \textit{retinal vasculature} distribution 
and 
positioning in the optic nerve head region, also known as the \textit{papillary 
region} \cite{chan_retinal_2017}.
Other physiological biomarkers include hemorrhaging in the papillary rim region 
and papillary atrophy \cite{greslechner_klinische_2016}.
\\
Optic disc and cup as well as blood vessels were chosen as the biomarkers as 
these are consistently present in various imaging techniques and are medically 
relevant, as illustrated in \Cref{fig:biomarker}. These 
biomarkers can provide important information about the 
progression of the disease and are widely used by ophthalmologists to diagnose 
glaucoma. 
\\
Fundus images and Optical Coherence Tomography (OCT) imaging are both commonly 
used in ophthalmology to diagnose various eye diseases, including 
glaucoma.
Fundus images are photographs of the back of the eye taken with a specialized 
fundus camera that consists of a microscope attached to a flash-enabled camera 
\cite{saine_ophthalmic_2002}.
OCT is a non-invasive imaging technique that 
uses light waves to capture high-resolution, cross-sectional images of 
the retina \cite{huang_optical_1991}.
In this thesis, fundus images were used as they provide a 
comprehensive view of the optic nerve head regarding size, appearance and 
shape,as well as the positioning and distribution of blood vessels in the 
papillary region. Additionally, fundus images are widely available and easier 
to obtain than OCT images, especially in resource-limited settings. 
Finally, fundus images are more cost-efficient than OCT images as they do not 
need special equipment.
\par
This thesis aims to develop a novel multi-step deep learning framework that can 
accurately segment images based on specific biomarkers and then evaluate the 
efficacy of this framework in detecting glaucoma. The 
framework first segments the images to identify the optic disc, cup, and blood 
vessels. The results of these segmentations are used, in addition 
to the fundus image, to detect glaucoma. Further, this approach is compared to 
a traditional deep learning approach that only uses the fundus image without 
the additional information from the segmentations.
\\
This thesis aims to determine if the inclusion of the 
segmentations in the deep learning model can improve its accuracy in the 
detection of glaucoma. The results of this study will provide 
valuable insights into the potential of multi-step deep learning frameworks for 
the automated analysis of fundus images and may have important implications for 
the future of glaucoma diagnosis and treatment as well as in the field of 
medical decision support systems.
