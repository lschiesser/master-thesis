\label{sec:implementation}
This section details and explains the implementation of each model and the 
final prediction pipeline. Due to the availability of different datasets for 
some biomarkers, the segmentation models segmenting the blood vessels and the 
remaining biomarkers are trained separately.

\section{Pipeline}
\label{sec:pipeline}
The final classification pipeline first preprocesses the fundus image by 
applying \textit{contrast limited adaptive histogram 
equalization} (CLAHE) to the image and cropping the fundus image to the 
papillary region if necessary. CLAHE is an image processing technique 
that enhances the contrast of an image by limiting the maximum pixel intensity. 
It works by dividing the image into small, overlapping tiles and equalizing the 
histogramof each tile separately \cite{zuiderveld_contrast_1994}. The fundus 
image is then segmented regarding the biomarkers and the blood vessels, 
creating two separate segmentation maps. The fundus image and the segmentation 
maps are then embedded using an ImageNet pre-trained ResNet34 architecture. 
Based on the non-embedded segmentation map, the ophthalmic metrics (vCDR, RDR, 
ISNT rule) are computed. The classifier then uses the embedded images and the 
ophthalmic metrics to make a classification.

\section{Blood Vessel Segmentation Model}
The blood vessel segmentation model was trained using the Blood Vessel Dataset. 
All images were preprocessed using CLAHE. The dataset was 
enlarged using  different combinations of image augmentation techniques, 
namely, rotating 
the images by at most 10° as well as changing the brightness, contrast, and 
saturation of the fundus images using the albumentations package 
\cite{buslaev_albumentations_2020}. Thus, 60 additional training images were created, totaling 80 training images. No image augmentation techniques were 
applied to the test and validation images.
\par
U-Net was employed to segment the blood vessels. As mentioned before, a 
ResNet34 architecture pre-trained on the ImageNet dataset was used as the 
encoder's path of the U-Net model. A learning rate of 0.001 and 
a mini-batch approach with a size of 1 were used due to the small sample size. 
Further three different loss functions were compared for this task: Dice loss, 
Focal loss, and Tversky loss.
\par
To find the best-performing versions of the model for all three loss functions, 
hyperparameter tuning was employed using Ray Tune \cite{liaw_tune_2018}. The 
hyperparameters that were investigated for this model were the number of epochs and 
the alpha values of the Tversky and Focal loss. The Dice loss has no tunable 
hyperparameters. The Tversky loss's alpha values are commonly greater than 
0.5 \cite{abraham_novel_2019}, and the range was set accordingly.
The Focal loss's alpha values weigh the different classes of the segmentation 
task. As already mentioned in Section \ref{sec:fl}, the weights can either be 
the inverse class frequency or hyperparameters. The different weights tested 
here are the inverse class frequency (rounded to two and one decimal) and 
complement class frequency. The weight assigned to the first entry corresponds 
to the background class, while the weight assigned to the second entry 
corresponds to the blood vessel class.  The number of epochs is kept relatively 
narrow to avoid overfitting and keep tuning in a reasonable time frame. An 
overview of all hyperparameters and their ranges is provided in Table 
\ref{tab:parameter-bloodvessel}. The hyperparameter tuning was evaluated for all 
three loss functions separately. The hyperparameter space was searched to 
maximize IoU scores. The model with the best-performing 
hyperparameter configurations was then re-trained and evaluated on the test 
dataset.

\begin{table}[ht]
\centering
\begin{tabularx}{\textwidth}{llX}
\hline
Loss function                 & Hyperparameter & Range \\ 
\hline
\multirow{2}{*}{Tversky loss} & Epochs         & 10, 15, 20, 25, 30, 35, 40 \\
                              & Alpha          & 0.5, 0.6, 0.7, 0.8, 0.9    \\ 
\hline
Dice loss                     & Epochs         & 10, 15, 20, 25, 30, 35, 40 \\ 
\hline
\multirow{2}{*}{Focal loss}   & Epochs         & 10, 15, 20, 25, 30, 35, 40 \\
                              & Alpha          & [1/0.78345, 1/0.21654], 
[1/0.78, 1/0.22], [0.22, 0.78] \\ 
\hline
\end{tabularx}
\caption{All possible hyperparameters used during hyperparameter tuning for 
the blood vessel segmentation model}
\label{tab:parameter-bloodvessel}
\end{table}

\section{Biomarker Segmentation Model}
The biomarker segmentation model was trained using the segmentation dataset. All 
images were preprocessed using contrast limited adaptive histogram 
equalization (CLAHE). The dataset was enlarged using  different combinations of 
image augmentation techniques, namely, rotating the images by at most 10° as 
well as changing the brightness, contrast, and saturation of the fundus images 
using the albumentations package. After applying the augmentation 
techniques, the resulting dataset used for the segmentation task consisted of 1137 images in the training set, 95 images in the validation set, and 120 images in the test set. Again, no augmentation techniques were applied to the test or 
validation images.
\par
U-Net was employed to segment the optic disc and cup. As already mentioned 
before, a ResNet34 architecture pre-trained on the ImageNet dataset was used as 
the 
encoder's path of the U-Net model. A learning rate of 0.001 was used. 
Further, three different loss functions were compared for this task: Dice loss, 
Focal loss, and Tversky loss.
\par
To find the best-performing versions of the model for all three loss functions, 
hyperparameter tuning was employed using Ray Tune \cite{liaw_tune_2018}. The 
hyperparameters in question are the number of epochs, the batch size, and the 
alpha values of the Tversky and Focal loss. The Dice loss has no tunable 
hyperparameters. As mentioned in section \ref{sec:tl}, the Tversky 
Loss's alpha value is used to weigh the relative importance of false positives 
and false negatives. Since \cite{abraham_novel_2019} used only alpha values 
greater than 0.5, the same was done here.
The Focal loss's alpha values weigh the different classes of the segmentation 
task. As already mentioned in Section \ref{sec:fl}, the weights can either be 
the inverse class frequency or hyperparameters. The different weights tested 
here are the inverse class frequency (rounded to two and one decimal) and 
complement class frequency. The weight vector used for Focal loss is composed of 
three entries, where the first entry represents the optic disc, the second entry 
corresponds to the optic cup, and the last entry is assigned to the background 
class. 
Finally, the range for the batch size is based on 
common values used in deep learning. The range for the number of epochs is also 
based on common values used in deep learning and is limited to 60 epochs to keep 
the tuning process in a reasonable time frame.  An overview of all hyperparameters 
and their ranges is provided in Table \ref{tab:parameter-biomarker}. The 
hyperparameter tuning was evaluated for all 
three loss functions separately. The hyperparameter space was searched to 
maximize IoU scores. The best performing 
hyperparameter configuration was then re-trained and evaluated on the test 
dataset. 

\begin{table}[ht]
\centering
\begin{tabularx}{\textwidth}{llX}
\hline
Loss function                 & Hyperparameter & Range \\ 
\hline
\multirow{3}{*}{Tversky loss} & Batch size     & 2, 4, 10, 20, 25 \\
                              & Epochs         & 10, 20, 30, 40, 50, 60 \\
                              & Alpha          & 0.5, 0.6, 0.7, 0.8, 0.9    \\ 
\hline
\multirow{2}{*}{Dice loss}    & Batch size     & 2, 4, 10, 20, 25 \\
                              & Epochs         & 10, 20, 30, 40, 50, 60 \\ 
\hline
\multirow{3}{*}{Focal loss}   & Batch size     & 2, 4, 10, 20, 25 \\
                              & Epochs     & 10, 20, 30, 40, 50, 60 \\
                              & Alpha          & [1/0.28, 1/0.1, 1/0.62], 
[1/0.3, 1/0.1, 1/0.6], [0.7, 0.9, 0.4] \\ 
\hline
\end{tabularx}
\caption{All possible hyperparameters used during hyperparameter tuning for 
the biomarker segmentation model}
\label{tab:parameter-biomarker}
\end{table}

\section{Classification Model}
\label{sec:classifier}
The classification model was trained using the classification dataset. This 
dataset is an extension of the segmentation dataset. Therefore, the 
preprocessing steps were the same as for the segmentation dataset. Furthermore, 
all images were embedded using a pre-trained ResNet34 to generate embeddings of 
all images.
\par
For this model, the aim was to investigate if additional information could 
improve the performance of a glaucoma classifier. Many networks 
primarily use the fundus image to classify glaucoma, but more information is
available, like the ophthalmic metrics or segmented biomarkers. Five 
experiments were devised to investigate 
whether this information can improve the classification performance. 
\par
\textbf{Experiment 1: Base experiment} In the base experiment, only the fundus 
image is used to make a classification. As already mentioned, the fundus images 
were embedded using a ResNet34 architecture pre-trained on the ImageNet 
dataset. The classification network is a neural network comprising three 
linear layers with input size 512, followed by hidden layers of size 256 and 
128, and an output layer of size 1. it is shown in  Figure \ref{fig:embed-img}.
\par
\textbf{Experiment 2:} In this experiment, the segmentation maps were used in 
addition to the fundus image for classification. 
The fundus images and the two created segmentation maps were embedded 
separately and then used as input for the classifier. The classifier is a 
neural network comprising three linear layers with input size 1536, followed by 
hidden layers of size 512 and 256, and an output layer of size 1, as shown in 
Figure \ref{fig:embed-seg}.
\par
\textbf{Experiment 3:} This experiment investigates whether metric inputs can 
improve the classification. This experiment's architecture is designed to 
incorporate both embedded fundus images and ophthalmic metrics as inputs. It 
consists of two pathways, the first being the image processor from Experiment 
2, which is responsible for processing the embedded fundus images. The second 
pathway comprises four layers with sizes 64, 128, 256, and 128, designed 
to process the ophthalmic metrics. The outputs from both pathways are then 
concatenated, and the concatenated tensor is passed through three additional 
layers with sizes of 256, 128, and 1 to generate the final output. The model 
architecture is also shown in Figure \ref{fig:embed-img-met}.
\par
\textbf{Experiment 4a:} In this final experiment, the embedded segmentation 
maps were concatenated with the embedded fundus images. The vertical 
cup-to-disc ratio and the rim-to-disc ratio were used as the second input to 
the classifier. The classifier is similar to the architecture in Experiment 3. 
Instead of the image processor from Experiment 2, it uses the model from 
Experiment 1. The rest of the architecture remains unchanged. The model is also 
shown in Figure \ref{fig:embed-seg-met}.
\par
\textbf{Experiment 4b:} As a variation of Experiment 4a, in addition to the 
vCDR 
and the RDR, the ISNT rule was included in the second input tensor. The first 
input tensor,i.e., the segmented fundus image and segmentation maps, was not 
changed. The classifier used for this experiment is also shown in Figure 
\ref{fig:embed-seg-met}.
\par
All classifiers were trained for 100 epochs using Binary Cross Entropy Loss. 
Adam \cite{kingma_adam_2017} was used as an optimizer with a learning rate of 
0.001. Furthermore, early 
stopping was used, saving the best model if the current validation loss was 
lower than the former lowest validation loss.
\begin{figure}[ht]
 \begin{subfigure}[t]{0.45\textwidth}
  \centering
  \includegraphics[width=0.6\textwidth]{models/embed-img}
  \caption{Only fundus images}
  \label{fig:embed-img}
 \end{subfigure}
 \hfill
 \begin{subfigure}[t]{0.45\textwidth}
  \centering
  \includegraphics[width=0.6\textwidth]{models/embed-seg}
  \caption{Fundus image + segmentation maps}
  \label{fig:embed-seg}
 \end{subfigure}
 \begin{subfigure}[t]{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{models/embed-img-met}
  \caption{Fundus images + ophthalmic metrics}
  \label{fig:embed-img-met}
 \end{subfigure}
 \hfill
 \begin{subfigure}[t]{0.45\textwidth}
  \centering
  \includegraphics[width=\textwidth]{models/embed-seg-met}
  \caption{Fundus images + segmentation maps + ophthalmic metrics}
  \label{fig:embed-seg-met}
 \end{subfigure}
\caption{Classifier architectures}
\end{figure}
