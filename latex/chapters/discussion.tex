In this chapter, the findings of this thesis are analyzed and discussed. The 
discussion will address the research questions, hypotheses, and objectives  presented in the earlier chapters and provides a critical evaluation 
of the methodology, results, and limitations of the thesis.
\section{Segmentation Models}
\subsection{Blood Vessel Segmentation}
Using hyperparameter optimization, three models with solid performance 
regarding the task of blood vessel segmentation in fundus images were created. 
All three models achieve an accuracy of around 93\%. However, an IoU 
score of just over 70\% and a Dice score of just over 83\% are not necessarily 
considered satisfactory. The DRIVE dataset is also hosted as a challenge, with 
over 2,000 participants submitting their approaches to blood vessel 
segmentation. The current leaderboard's\footnote{The leaderboard is available 
at: 
\url{https://drive.grand-challenge.org/evaluation/challenge/leaderboard/}} best 
submission has achieved a Dice score of 97\%, demonstrating the 
state-of-the-art performance on this dataset. Unfortunately, no additional 
details about the methods used to achieve this result are available. Despite the 
small dataset, the models presented in this study performed well, with results 
comparable to many submissions on the leaderboard that achieved a Dice score of 
around 83\%.  
Furthermore, a good performance can be visually confirmed by comparing the 
generated segmentation masks with the expert annotations (see Figure 
\ref{fig:bv-comp}).
Therefore, it can be concluded that U-Net works well even with very limited 
data in combination with hyperparameter tuning.
\\
Nevertheless, the reliability of the validation metrics as the decision metrics 
during hyperparameter tuning might be subject to improvements. The 
validation split of the dataset only contains five images. Therefore, it is 
possible that the informative value of these metrics is not the same 
compared to larger validation datasets. Therefore, it is important to exercise 
caution when interpreting the validation metrics obtained during hyperparameter 
tuning for smaller datasets. Visual inspection of segmentation results and 
comparison with results from other architectures should be included in this 
process. To address these limitations, one potential solution could be to 
increase the size of the dataset by merging multiple datasets together or 
supplementing the datasets with data from clinical trials to acquire more images 
with ground truth masks.
\\
Moreover, it could be beneficial to the whole training process to increase the 
size of the training dataset used. A vast amount of image augmentation was used 
to increase the size of the training dataset from 20 to 80 images. In some 
cases, this can lead to overfitting and a worse performance 
\cite{shorten_survey_2019}. Despite the potential for overfitting, image 
augmentation can be a useful tool to increase the size and variability of a 
limited training dataset. The augmentation techniques were carefully selected 
to introduce variability while maintaining the integrity of the original 
images. Creating a bigger dataset is especially difficult for the task of blood 
vessel segmentation. Annotating retinal vasculature is a complex task due to 
the varying size and visibility of blood vessels on the retina. As a result, 
there are few public datasets that often include only a few images.

\subsection{Biomarker Segmentation}
Three models with good performances were created. While all of the models 
achieve an accuracy above 90\%, it is clear that the Dice 
and Tversky loss models performed better than the Focal loss. A Dice score of over 
90\% and an IoU score of over 84\% signify a strong performance. Even 
when visually inspecting and comparing the prediction to the ground truth mask, 
it is clear that both the Dice and the Tversky model segment the biomarkers 
reliably. \\
Nonetheless, looking at the training and validation loss of the models in 
Figures 
\ref{fig:seg-dice-training}, \ref{fig:seg-focal-training}, 
\ref{fig:seg-tversky-training}, it is apparent that all of the models start to 
overfit soon on the data as the validation loss begins to grow. However, 
the change in validation loss is insignificant and only noticeable in the 
second decimal place. \\
Similar to the blood vessel segmentation model, it could be beneficial to 
include more edge cases during training for the biomarker segmentation model. 
Edge cases, such as fundus images with poor illumination and contrast, as well 
as spectral anomalies like opacities or discoloration due to comorbidities, can 
reflect the true distribution of the data better. Although the current dataset 
already includes some of these cases, it would be interesting to investigate 
whether the segmentation performance can be further improved by including more 
edge cases during training.

\section{Classification Model}
The results of the experiments described in Chapter \ref{sec:classifier} 
support the hypothesis that guided this thesis. 
Especially the inclusion of the segmentation maps in addition to the fundus 
image (Experiment 3) improves the classification notably compared to the fundus 
image only experiment (Experiment 1). Further, the inclusion of ophthalmic 
metrics also improved the classification performance, although not as much as 
the segmentation maps. 
When combining the fundus image, segmentation maps, and the ophthalmic 
metrics (Experiment 4a), the classifier improves compared to Experiment 1 but 
not compared to Experiment 3. When including the ISNT areas in the ophthalmic 
metrics (Experiment 4b), the classification performance improves especially 
looking at the (balanced) accuracy and sensitivity compared to experiments 
1, 4a, and 3. This suggests that the classifier's performance is more enhanced 
when the vCDR, RDR, and ISNT information is included along with the 
segmentations and fundus images, as opposed to using only vCDR, RDR with 
segmentations and fundus images or just the fundus image. This is 
surprising since the ISNT rule and the calculation of the ISNT areas are 
side-specific, i.e.; the eye position (i.e., right or left eye) has to be 
included during the computation of the rule to order the areas correctly. 
However, the RIM-ONE DL dataset has no information about the eye position. Yet, 
to compute the ISNT rule, an eye position has to be assumed. Therefore, the left 
eye was assumed as most of the fundus images from the SALUS study were left 
eyes. The classifier algorithm (as well as the segmentation models) did not get 
any information about the side of the eye. It is possible that the classifier 
was able to detect the side of the eye implicitly from the fundus image and 
segmentation maps in combination with the ISNT areas. Further testing, including 
the eye position for the correct computation of the ISNT areas and the 
subsequent classification, is needed to prove this theory. \\
Another potential hypothesis regarding this effect is that the classifier 
learned to 
down-weigh the importance of the areas of the nasal and temporal quadrants. A 
study by \citeauthor{poon_isnt_2017} \cite{poon_isnt_2017} found that the ISNT 
rule only applies to 37\% of fundus images. Neglecting the nasal and temporal 
quadrants increased the rule's validity to up to 
76.4\% \cite{poon_isnt_2017}. Therefore, focusing on the interior and 
superior quadrants of the ISNT areas and disregarding the nasal and temporal 
quadrants could lead to a better classification. More experiments where 
different areas of the ISNT rule are left out during the training process could 
shed more light on this aspect.
\par
Given the medical context, it is reasonable to prioritize the 
sensitivity and specificity over the accuracy of the models, as they are 
particularly significant for assessing the efficacy of medical tools. 
Sensitivity refers to the ability of a model to correctly identify positive 
cases, while specificity refers to the ability of a model to identify 
negative cases correctly. In medical applications, false positives and false negatives can 
have significant consequences, so it is important to evaluate models not only on 
their accuracy but also on their sensitivity and specificity. 
Although Experiment 4b's classifier achieves the highest 
accuracy and sensitivity, its specificity is lower than in Experiment 3. 
Similarly, Experiment 4a achieves a higher accuracy and specificity than 
Experiment 3 but has a lower sensitivity. Therefore, the classifier's 
performance can be regarded as less stable compared to Experiment 3. They can 
identify true negative or true positive cases respectively less reliably. 
Accordingly, Experiment 3's classifier can be regarded as more reliable in its 
performance since it has a high accuracy as well as a matching sensitivity and 
specificity rate. It is thereby able to identify true positive and true 
negative cases more reliably. This also means that the addition of metrics is 
not necessarily beneficial to the classification process.
\par 
Looking at the plots in Figures \ref{fig:classifier-exp1-training} 
to \ref{fig:classifier-exp4b-training}, it is apparent that all classifiers 
start to overfit rather quickly. There are several possible reasons for this. 
First, it is possible that the dataset is not sufficient to train the 
classifiers. The dataset only has 379 training images, applying  
augmentation results in 1137 training images. The size of the dataset could be 
insufficient for training. A larger dataset could provide more variability in 
the data to facilitate a better training process. Although applying image 
augmentation techniques to the dataset creates more and slightly different 
images, it is possible that the dataset now lacks the variability a network 
needs to still be able to generalize \cite{shorten_survey_2019}. Therefore, it 
might be beneficial to include more images in the dataset or to apply more 
diverse augmentations. \\
Another difficulty during the training process may be the segmentations produced 
by the segmentation models. Especially the blood vessel segmentation model may 
produce bad segmentation maps since the dataset used for training this model 
does not 
include the edge cases the classification dataset contains. The blood vessel 
dataset's fundus images are all well-lit and show no opacities or discoloration 
due to other comorbidities; the classification dataset, however, does. 
Therefore, some of the blood vessel segmentation maps may include little or even 
no information at all. This could impact the training process negatively. \\
The classifiers' input consists of the pre-embedded fundus images and 
segmentation 
maps. Especially in the case of the classifiers in Experiments 3 and 4, the 
dimensionality of this input vector is very high with a length of $d=1536$. As 
the 
classifiers themselves are relatively shallow, the dimensionality is reduced 
very suddenly. Their capacity to learn complex features is limited, which is 
further compounded by the small training dataset, making it more challenging to 
learn high-dimensional features. As a result, shallow 
networks may not fully capture the discriminative features. 
To mitigate the negative impact of high dimensionality in shallow networks, 
dimensionality reduction techniques can be used. Dimensionality reduction aims 
to reduce the number of features while preserving the most relevant 
information. 
This can reduce the number of parameters in the model, simplifying the 
process of training a shallow network. Principal Component Analysis 
\cite{mackiewicz_principal_1993}, Linear Discriminant Analysis 
\cite{mika_fisher_1999}, and t-SNE \cite{van_der_maaten_visualizing_2008} are 
some of the popular dimensionality reduction techniques used in medical image 
analysis. As the thesis primarily concerns the incorporation of additional 
information and several of these methods involve the inclusion of additional 
hyperparameters for hyperparameter optimization, they were not employed in this 
study.
\par
Exploratory studies with the classifier included testing a network that 
included the ResNet architecture with a custom classifier head. The network's input was intended to be either the fundus image alone or a 5-channel 
image consisting of the fundus image 
and the segmentation maps. Moreover, it was planned to provide the metrics as an 
extra input through a secondary pathway. The different classifier 
variations were analogous to the experiments given in Chapter 
\ref{sec:classifier}. Unfortunately, the 
classifier did not learn as soon as the 5-channel image was used as an input. 
All metrics remained stagnant during the 100 epoch long training process, 
suggesting that the network did not learn. Although multiple investigations were carried out, it is not entirely clear why this happened. The ResNet 
structure in the model was entirely frozen, meaning that none of the layers and 
their 
weights were set to be adapted during backpropagation. Accordingly, only the 
classifier head was set to be trained. This could have led to a problem with 
the backpropagation algorithm used in the implementation provided by Pytorch 
\cite{paszke_pytorch_2019}. 
Further, an investigation of the gradient values during backpropagation 
revealed 
vanishingly small gradients, which affected the training process negatively.
Unfreezing all layers, including the ones in the ResNet part of this classifier, 
did not improve the training process. Indeed, since the problem of vanishing 
gradients already occurred when only the classifier head was trained, the 
problem of vanishing gradients persisted in this case, too. In general, it was 
challenging to find literature on the extension of architecture pre-trained 
on 3-channel images to 5-channel images. Therefore, this is also 
hypothesized to be the primary error source. Another reason for this problem might 
be the saturation of layers, 
especially when re-training the whole architecture 
\cite{richter_feature_2021, richter_size_2021}. Layer saturation measures 
the proportion of spatial dimensions occupied by the information in a layer 
$l$. 
It can act as an indicator that shows the fraction of useful dimensions 
in the output space. A high saturation, especially in later layers of the 
network, 
corresponds to a high activity in these layers. A high activity in a 
convolutional layer is associated with the integration of new information, which 
can improve the classification performance. By plotting the saturation 
level of each network layer, a network's inference dynamic can be analyzed and 
compared to other networks \cite{richter_feature_2021}. To investigate the 
reason why the 5-channel architecture did not learn, further research could 
compare the saturation levels of the 3-channel ResNet model with those of the 
5-channel ResNet model.

\section{Pipeline}
The final classification pipeline is a combination of multiple, individually 
trained models. Unfortunately, it was not possible to train all models in an 
end-to-end fashion because no dataset included all biomarkers and information required for this framework. Finding 
data for the blood vessel segmentation was particularly difficult as there are 
only 
few public datasets available that only include segmentation annotations. A common dataset for optic disc, cup, and blood vessels was not 
known at the point of the model development. However, there are some 
advantages to training these models individually.  
\\
One major advantage is the flexibility of individual training. Architecture 
hyperparameters and optimization algorithms can be adapted to a specific task. 
This 
allows for fine-tuning each model individually, which can lead to a better 
overall performance. Moreover, it also simplifies the training process and 
makes it easier to debug errors. By 
training models individually, they are more interpretable since each model's 
output and its contribution to the final prediction can be investigated.
\\
Major drawbacks of individual training are a more complicated integration 
and problems during communication. Since each model is trained individually, the 
outputs of each model have to be integrated to fit the next processing step. 
This requires additional steps such as feature extraction, selection, and 
fusion. Further, the models cannot interact with each other during 
individual training processes, which they need to when put together in the final 
framework. This can add additional complexity and potentially increase the 
computational 
overhead.
\\
An end-to-end approach could provide a more straightforward, optimized, and efficient 
training process. In an end-to-end fashion, all models are trained at the 
same time, which can be computationally more efficient since models can share 
parameters and computations. Further, it simplifies the training process by 
eliminating intermediate steps such as feature extraction and fusion. Finally, 
during end-to-end, training the optimization algorithm can adjust the entire 
system to minimize the overall loss function, e.g., a composite loss function 
\cite{bozic_end--end_2020}, which can lead to a better 
optimized overall system. End-to-end training has 
some drawbacks, including a higher complexity than individual training and limited 
flexibility in terms of architecture, hyperparameters, and optimization 
algorithms. Additionally, the difficulty of interpreting the output of each model 
and its contribution to the final prediction is also a concern. Another issue 
that can arise during end-to-end training is vanishing gradients, which can 
occur when the gradients used to update the model's weights become very 
small, making it difficult for the model to learn from the data.
Nevertheless, exploring an end-to-end trained version of this framework would be 
a promising direction for future research.

\section{Outlook}
\label{sec:outlook}
The models developed in this thesis were part of the SALUS study 
\cite{oldiges_salusnon-inferiority_2022}. An aspect of 
this study was to include artificial intelligence during the glaucoma diagnosis 
process. 
As part of SALUS, an online patient record system was developed. It 
provides patients access to all their medical data, including imaging 
procedures, eye examination protocols, and blood tests. Additionally, it 
provides a reading center accessible to ophthalmologists.
A reading center in ophthalmology is a specialized facility or platform that 
provides expert interpretation and analysis of medical images, specifically in 
the field of ophthalmology. Reading centers play a critical role in clinical 
trials for new treatments and drugs for eye diseases, as they provide an 
independent and standardized evaluation of the medical images generated during 
the trial.
In clinical trials, images of the eye, such as retinal photographs, optical 
coherence tomography (OCT) scans, and visual field tests are collected from 
patients to evaluate the effectiveness and safety of new treatments. These 
images are then sent to a reading center for review and analysis by a team of 
specialized ophthalmologists who are experts in the specific disease being 
studied.
The reading center uses standardized protocols to evaluate the images and 
provide an objective and consistent assessment of the patient's disease status. 
The evaluation may include measurements of the size of lesions, changes in the 
thickness of the retina, or other indicators of disease progression or 
improvement \cite{tan_role_2015, noauthor_ukm_nodate}.
As part of this reading center, fundus images are also evaluated during 
the diagnosis process. Currently, SALUS is evaluating the application of the 
models developed in this thesis as part of the diagnosis process. There are 
different application scenarios.
\paragraph{Application Scenario 1} During the reading center process, 
ophthalmologists also review fundus images for structural anomalies. To aid 
professionals during the review, the segmentation models could be used in the 
reading center. The biomarker and blood vessel segmentation maps could be 
helpful for clinicians to get an overview more quickly. The modularity of the 
architecture used in this thesis allows for easy application to this scenario 
as the individual models were trained separately. This makes  
integrating the segmentation models into the reading center workflow easier.
\paragraph{Application Scenario 2} Another aspect of the reading center 
developed during the SALUS study is a peer review process of patient data and 
associated diagnoses. Two ophthalmologists review each patient's data 
independently and make a diagnosis. An application scenario could be that the 
classifier developed in this thesis could act as a third reviewer and make a 
diagnosis based on the pipeline described in Chapter \ref{sec:pipeline}. The 
classifier could provide a third opinion in case of disagreement between the 
two human reviewers or trigger a new round of reviews in case the algorithm 
predicts a different diagnosis than the two human reviewers.
\par
Using these models in a platform like a reading center raises several 
ethical and legal considerations.
Any software that is intended for the diagnosis, prevention, monitoring, or 
treatment of diseases is considered a medical device and falls under the 
Medizinprodukterecht-Durchführungsesetz (MPDG) \cite{noauthor_gesetz_2021}, 
which implements the European Union's regulation on medical devices (MDR) 
\cite{noauthor_regulation_2020} and in-vitro diagnostics (IVDR) 
\cite{noauthor_verordnung_2022}. The MPDG contains requirements for clinical 
evaluation, risk assessment, technical documentation, and market surveillance of 
medical devices. A key requirement is the implementation of a certified quality 
management system that ensures compliance with relevant regulations and 
standards \cite{noauthor_major_nodate}. In addition, the software must meet 
certain security requirements to ensure its reproducibility and performance 
under normal and abnormal conditions 
\cite{gerhart_medizinprodukterecht-durchfuhrungsgesetz_2022}. Further, a 
technical documentation that covers all aspects of the software development and 
operation is required. This documentation must include information on the 
software's architecture, training process, datasets, verification and validation 
methods, user instruction, and post-market surveillance 
\cite{noauthor_mdr_nodate, 
gerhart_medizinprodukterecht-durchfuhrungsgesetz_2022}. To comply with the MPDG, 
medical software must also meet the requirements of harmonized norms that 
provide guidance on the design, development, risk management, validation, 
installation, and maintenance of software as a medical device. Examples of such 
norms include IEC 62304 for software life cycle processes and IEC 82304 for 
health software \cite{noauthor_mdr_nodate}. The software can only be 
distributed after it underwent a conformity assessment procedure and obtained a 
CE marking. The MPDG also establishes penalties for non-compliance with these 
requirements \cite{reinsch_regulatorische_2023, noauthor_mdr_nodate}. The MPDG 
is subject to change or adaptation in the future if new regulations or 
standards are introduced at the EU level. One such regulation that might affect 
software as a medical device is the proposed Artificial Intelligence Act (AIA), 
which aims to regulate high-risk AI systems across various sectors. The AIA 
might introduce additional requirements for transparency, accountability, and 
human oversight of AI systems used for healthcare purposes 
\cite{reinsch_regulatorische_2023}.
\\
The most important ethical concern is the potential of algorithmic bias 
\cite{hoffman_artificial_2020, ghassemi_medicine_2022}, which is also addressed 
in the MPDG. Deep learning models are usually trained on vast amounts of data, 
but this data may not be representative of the patient population. 
This can lead to biases in the results where underrepresented groups are most 
impacted. Thus, it is crucial to ensure that any models used in reading centers 
are regularly audited for bias and that any biases are identified and addressed.
\\
Additionally, there is a risk of overreliance on these models. Deep learning 
and machine learning models are only as good as the data they are trained on. 
Accordingly, human oversight is necessary to ensure the accuracy and 
reliability of the results. In case of an application, reading centers must 
ensure that these models are used to supplement rather than replace human 
expertise and that human oversight is integrated into the analysis and 
diagnosis process.\\
Lastly, it is important to consider the potential for automation to replace 
human labor. While this is not the case in the application scenarios for the 
SALUS reading center, it is still a matter that has to be discussed. These 
models can improve the efficiency and accuracy, but they also have the 
potential to dis- or replace skilled workers in the field of medical image 
analysis.  Thus, it is necessary to consider the social and economic impact of 
the implementation of these models and to ensure that they are integrated into 
existing workflows responsibly and equitably. They should rather be 
seen as helpful tools to reduce workers' workload than replace them.
