This chapter introduces the materials and methods used to develop the proposed 
multi-step framework.
\section{Data and Annotation}
Several datasets were used to train the individual models for this multi-step 
framework. We will call them \textit{Blood Vessel}, \textit{Segmentation}, and 
\textit{Classification Datasets} in the following section.
\subsection{Segmentation Dataset}
The \textit{Segmentation Dataset} is used to train the model segmenting the 
optic disc 
and cup in the papillary region of the retina. The dataset contains all images 
from the RIM-ONE DL dataset \cite{batista_rim-one_2020} and retinal fundus 
images from patients enrolled in the SALUS study 
\cite{oldiges_salusnon-inferiority_2022}. The RIM-ONE DL dataset consists of 
313 retinographies from healthy subjects and 172 retinographies from patients 
with glaucoma. All images have been evaluated by two experts at the University 
Hospital of the Canary Islands and include expert 
segmentations of the optic disc and cup. The images in the RIM-ONE DL dataset 
were all cropped to the papillary region. Additionally, 109 images 
from patients from the SALUS study were included, all of whom were diagnosed 
with glaucoma. The images were first cropped to the papillary region and then 
annotated by an ophthalmologist from the Department of Ophthalmology at the 
University Hospital Münster using CVAT \cite{sekachev_opencvcvat_2020}, a 
video and image annotation tool. The 
segmentation dataset, therefore, consists of 281 fundus images from patients 
with glaucoma and 313 images from healthy patients.
\subsection{Blood Vessel Dataset}
The \textit{Blood Vessel Dataset} is used to train our model segmenting the 
retinal blood vessels of the papillary region. The dataset comprises the 
full DRIVE dataset \cite{staal_ridge_2004} and five random pictures from the 
STARE dataset 
\cite{hoover_locating_2000}. The DRIVE dataset contains 40 images
taken from the retinas of human subjects with corresponding expert-generated 
ground truth segmentation maps indicating the location of blood vessels in each 
image. It is split into 20 training and 20 test images. Seven of the 40 images 
show mild signs of diabetic retinopathy; three were included in the 
training set and four in the test set. The five pictures from the STARE 
dataset, all taken from healthy subjects, were used as a validation 
dataset. All images and their corresponding ground truth masks were manually 
cropped to the papillary region.
\subsection{Classification Dataset}
The \textit{Classification Dataset} is used to train the classification models. 
It consists of the Segmentation Dataset with additional information. First, the 
blood vessel extraction model was employed to segment 
the blood vessels in each image. Then the optic disc and cup segmentations were 
used to compute \textit{ophthalmic metrics}, specifically, the 
\textit{vertical cup-to-disc ratio} (vCDR), the \textit{rim-to-disc} ratio 
(RDR), and the \textit{ISNT rule}, which are further explained in Chapter 
\ref{sec:ophthalmic-metrics}. The 
fundus image, expert optic disc and cup, as well as the blood vessel 
segmentation were then embedded using a pre-trained ResNet-34 
\cite{he_deep_2015} and then 
concatenated resulting, in a tensor of shape \textit{[1, 1536]}. Each embedded 
image accounts for a third in the following order: fundus image, biomarker 
segmentation, and blood vessel segmentation. Finally, we created a binary 
classification label that indicates whether the current subject has glaucoma or 
not. The dataset followed the same split for test, training, and validation as 
the Segmentation Dataset.
\section{Methods}
This section details the techniques and architectures used to create and train 
the segmentation and classification models.
\subsection{U-Net}
\label{sec:u-net}
\textit{U-Net} is a powerful convolutional neural network commonly used for 
biomedical image segmentation \cite{ronneberger_u-net_2015}. Its architecture 
is based on an encoder-decoder network. The encoder, which is also sometimes 
called the 
contractive path, downsamples the input image, while the decoder, also 
sometimes 
called the expansive path, upsamples the encoded representation producing a 
segmentation mask. U-Net’s encoder and decoder networks are connected by a 
series of concatenative skip connections, i.e., identity mappings across layers 
which help to preserve spatial information and improve the accuracy of the 
segmentation. These connections pass information from 
the encoder to the decoder at multiple scales, allowing the decoder to make 
more accurate predictions by considering both global and local 
information from the input image \cite{chen_encoder-decoder_2018}. U-Net can 
already achieve results comparable with sliding-window based convolutional 
networks when trained on an extremely small dataset of a few hundred images. 
When adding data augmentation and preprocessing, it 
outperforms existing state-of-the-art methods on several biomedical image 
segmentation challenges \cite{ronneberger_u-net_2015}. A pretrained architecture 
can be employed as U-Net’s encoder path to improve the model’s general 
performance as measured by \textit{accuracy} and \textit{Intersection over 
Union (IoU)} score, which are explained in Chapter \ref{sec:metrics}
\cite{iglovikov_ternausnet_2018}. We chose a ResNet-34 architecture pretrained 
on the ImageNet dataset as our encoder path.
\begin{figure}[ht]
 \centering
 \includegraphics[width=0.8\textwidth]{u-net-architecture.png}
 \caption{U-Net architecture}
\end{figure}

\subsection{Transfer Learning}
\textit{Transfer learning} is a machine learning technique that applies a model 
trained on one task on another task. 
It is a powerful approach that can significantly reduce the amount of labeled 
data and computational resources required to train a model, as well as improve 
the model's performance on the target task \cite{zhuang_comprehensive_2020}.
\par
There are several ways to perform transfer learning. This section focuses on 
the \textit{fine-tuning approach} since it is used in this thesis. To use 
fine-tuning, a pre-trained network with pre-existing weights from a different 
task is re-trained on the target task with labeled data. The re-training of the 
network can either 
affect all layers of the model or only a subset of the layers by freezing some 
of the layers. Further, the pre-trained model is frozen completely and 
additional trainable layers are added to the existing model. Fine-tuning can 
improve a model's performance on the target task, especially when the target 
task is closely related to the source task and the 
labeled data for the target task is limited \cite{yang_hyperparameter_2020, 
zhuang_comprehensive_2020}.
\par 
Transfer learning is a useful technique for reducing the amount of 
labeled data and computational resources required to train a machine learning 
model, as well as improving the performance and generalization of the model on 
the target task.
\subsection{Hyperparameter Tuning}
\textit{Hyperparameter Tuning}, or hyperparameter optimization, is a technique 
to find 
the optimal hyperparameter configuration of a model to improve its 
performance on a specific task \cite{hutter_hyperparameter_2019}. 
\textit{Hyperparameters} are parameters that cannot be learned during training 
and are 
therefore set before 
training the model \cite{yu_hyper-parameter_2020}. Examples of hyperparameters 
include the number of training epochs, the batch size, the learning rate, and 
parameters of loss functions and optimizers. 
\par
There are several approaches to hyperparameter tuning, including manual tuning, 
grid search, random search, or Bayesian optimization. This section focuses on 
grid search since it is the approach used in this thesis. \textit{Grid search} 
is a systematic approach to hyperparameter tuning that exhaustively searches 
the hyperparameter space by testing all possible combinations within specified 
ranges \cite{hutter_hyperparameter_2019}. This brute force approach leads to 
the most accurate predictions. However, it suffers from a combinatorial 
explosion which means that the computational complexity of the tuning 
process increases exponentially with the number of hyperparameters and the 
number of possible values they can take on \cite{yang_hyperparameter_2020}. 
One way to counteract this issue is by using early stopping techniques like 
the \textit{Asynchronous Successive Halving Algorithm} (ASHA). ASHA is a 
bandit-based algorithm that efficiently explores a large configuration space by 
eliminating low-performing configurations early and allocating more resources 
to higher-performing ones. ASHA is suitable for parallel and distributed 
settings because it does not require synchronization or coordination among 
workers \cite{li_system_2020}. Despite its potential drawbacks, grid search is 
still a widely used method for hyperparameter tuning.
\subsection{Loss Functions}
Different loss functions play a crucial role in achieving a good model 
performance. This section provides an overview of various loss functions that 
are used in this thesis.
\subsubsection{Binary Cross Entropy Loss}
The \textit{Cross Entropy loss} function is one of the most commonly used loss 
functions 
in machine learning. As already 
in the name, the Cross Entropy loss is based on the concept of entropy, a 
measure of uncertainty. In the context of Cross 
Entropy loss, cross entropy is used to measure the uncertainty 
between the true and the predicted probability distribution 
\cite{murphy_machine_2012}. 
% check this statement
A low entropy indicates a high degree of certainty, while a high entropy 
indicates a high degree of uncertainty. By minimizing the Cross Entropy loss, 
the cross entropy between the true and the predicted probability distribution 
is minimized and the certainty of the model's predictions is maximized.
%-------
For the binary case, the Binary Cross Entropy is  calculated as shown in 
Equation \ref{eq:BCE}, where $y$ is the correct class label and $p$ is the 
predicted class probability. 
\begin{equation}
 L_{BCE}(p, y) = -y ln(p) - (1-y)ln(1-p)
 \label{eq:BCE}
\end{equation}
It can be simplified as shown in Equation \ref{eq:BCE-simple}.
\begin{equation}
 L_{BCE}(p, y) =
 \begin{cases}
  -ln(p), & \text{for } y = 1 \\
  -ln(1-p), & \text{otherwise}
 \end{cases}
 \label{eq:BCE-simple}
\end{equation}
The Binary Cross Entropy loss can be extended to handle $N$ classes. 
First, the loss is calculated for each class separately. The individual class 
losses are then summed up, resulting in the final loss value as shown in 
Equation \ref{eq:CE}
\begin{equation}
 L_{CE}(p, y) = -\sum_{i=1}^N y_i ln(p_i)
 \label{eq:CE}
\end{equation}

\subsubsection{Focal Loss}
\label{sec:fl}
The \textit{Focal loss} function is an extension of the Cross Entropy Loss. It 
is designed with extreme class imbalances and one-stage object detectors in 
mind \cite{lin_focal_2018}. It is especially helpful in cases with imbalanced 
classes. Focal loss employs a technique called down-weighting to focus on 
hard, misclassified examples. Down-weighting reduces the influence of easy to 
predict examples resulting in more attention on hard to predict examples. This 
is done via a modulating factor $\gamma$. In addition to down-weighting, each 
class can receive a weight $\alpha_c$ weighting their importance. Usually, high 
weights are assigned to rare classes  and low weights to common or dominating 
classes. 
The $\alpha$ weights can be determined by the inverse class frequency or as a 
hyperparameter \cite{jadon_survey_2020}.
\begin{equation}
 FL(p_t) = -\sum_{c=1}^{C} \alpha_{t,c} (1-p_{t,c})^\gamma log(p_{t,c})
 \label{eq:FL}
 \end{equation}

\subsubsection{Dice Loss}
\label{sec:dl}
The \textit{Dice loss} function is based on the widely used Dice Score (DS) 
evaluation 
metric, also known as F1 score (see Chapter \ref{sec:dice}). It is used to 
assess the segmentation performance when a ground truth 
segmentation is available. It is the harmonic mean between recall and 
precision, and thus a measure for the overlap of the predicted 
mask and the ground truth \cite{sudre_generalised_2017}. To compute the Dice 
Loss, the Dice Score is subtracted from 1, as shown in Equation \ref{eq:DL}. 
The 
Dice loss can be computed for N classes where $p$ is the predicted class 
probability and $y$ is the ground truth segmentation map. $\epsilon$ is used to 
avoid the numerical issue of dividing by 0.
\begin{equation}
 DL(p, g) = 1 - \frac{2TP}{2TP + FP + FN} \equiv 1 - \frac{2 \sum_{i}^{N} p_i 
g_i + \epsilon}{\sum_{i}^{N} p_i^2 + 
\sum_{i}^{N} g_i^2 + \epsilon}
\label{eq:DL}
\end{equation}
The Dice Score and, therefore, the Dice loss considers information at the 
global and local scale. The denominator considers the total number of boundary 
pixels at the global level, while the numerator considers the overlap between 
two 
sets of sets at the local scale. The Dice loss is often used in medical image 
segmentation due to the properties mentioned above because it 
considers both recall and precision. In some cases, it is crucial to fully 
capture pathological structures, such as in tumor detection, where recall is of 
greater importance. This is where the Tversky loss with corresponding 
parameters 
becomes more relevant.
\subsubsection{Tversky Loss}
\label{sec:tl}
The \textit{Tversky loss} function is based on the Tversky Index, an asymmetric 
similarity measure between two sets \cite{abraham_novel_2019}. It is 
particularly useful when there is a need to balance the relative importance of 
false positives and false negatives (see Chapter \ref{sec:metrics}). Equation 
\ref{eq:TI} defines the Tversky Index (TI). 
\begin{equation}
\begin{aligned}
TI_{\alpha, \beta}(p,g) = & \frac{TP}{TP + \beta FP + \alpha FN} \\ 
  & \equiv 
\frac{\sum_{i}^{N} p_i g_i + \epsilon}{\sum_{i}^{N} p_i g_i + 
\beta \sum_{i}^{N} p_i (1 - g_i) + \alpha \sum_{i}^{N} (1 - p_i) + \epsilon}
 \label{eq:TI}
 \end{aligned}
\end{equation}
The Tversky Index uses the $\alpha$ and $\beta$ parameters to control the 
relative importance of false positives and false negatives 
\cite{salehi_tversky_2017}. 
The Tversky loss is defined as seen in Equation \ref{eq:TL} and ranges from 0 
to 
1. The Tversky loss allows for more fine-grained control over the trade-off 
between false positives and false negatives using the $\alpha$ and $\beta$ 
parameters. 
When $\alpha < \beta$, the loss function puts more emphasis on reducing false 
positives, and when $\alpha > \beta$, it puts more emphasis on reducing false 
negatives. The beta parameter is 
often calculated by subtracting alpha from one. If $\alpha = \beta = 0.5$ the 
Tversky Index is called the Dice Score and in case of $\alpha = \beta = 1$ it 
is called the Jaccard index (also known as Intersection over Union score) 
\cite{gabor_tversky_2022}. The Tversky loss is often used where foreground and 
background classes are highly imbalanced, which is often the case in medical 
image segmentation tasks.
\begin{equation}
 TL(p, g) = 1 - TI_{\alpha, \beta}
\label{eq:TL}
\end{equation}

\section{Metrics}
\label{sec:metrics}
The following section introduces and defines the metrics used to evaluate the 
training process during hyperparameter tuning and the performance of the final 
models on test data. The following terms will be used in the 
equations to describe them: \textit{True Positive}, \textit{False Positive}, 
\textit{False Negative}, and \textit{True Negative}. They are further 
characterized in Table \ref{tab:confusion-matrix}. 
\begin{table}[ht]
\centering
\begin{tabular}{ll|c|c|}
\cline{3-4}
 &  & \multicolumn{2}{c|}{True condition}                                       
 
       \\ \cline{3-4} 
 &  & \multicolumn{1}{l|}{Condition positive} & \multicolumn{1}{l|}{Condition 
negative} \\ \hline
\multicolumn{1}{|l|}{\multirow{2}{*}{\begin{tabular}[c]{@{}l@{}}Predicted\\ 
condition\end{tabular}}} & \begin{tabular}[c]{@{}l@{}}Condition\\ predicted\\ 
positive\end{tabular} & \begin{tabular}[c]{@{}c@{}}True Positive\\ 
(TP)\end{tabular} & \begin{tabular}[c]{@{}c@{}}False Positive\\ 
(FP)\end{tabular} \\ \cline{2-4} 
\multicolumn{1}{|l|}{}                                     & 
\begin{tabular}[c]{@{}l@{}}Condition\\ predicted\\ negative\end{tabular} & 
\begin{tabular}[c]{@{}c@{}}False Negative\\ (FN)\end{tabular} & 
\begin{tabular}[c]{@{}c@{}}True Negative\\ (TN)\end{tabular}  \\ \hline
\end{tabular}
\caption{Confusion matrix for binary classification}
\label{tab:confusion-matrix}
\end{table}
\\
\subsection{Accuracy}
\textit{Accuracy}, or \textit{Pixel Accuracy} in the case of segmentation, 
describes the 
closeness of a predicted label to its ground truth label 
\cite{akobeng_understanding_2007}. In case of semantic 
segmentation, it is the percentage of correctly classified pixels in an image 
\cite{muller_towards_2022}. 
It is calculated as shown in Equation \ref{eq:accuracy}. 
\begin{equation}
 accuracy = \frac{TP + TN}{TP+FP+FN+TN}
 \label{eq:accuracy}
\end{equation}
While a standard metric, the pixel accuracy may not be the most appropriate 
measure to use as it can be heavily influenced by class imbalances.
In cases where the background pixels predominate, an image with only background 
pixels will still achieve a high accuracy \cite{muller_towards_2022}.
\subsection{Intersection over Union score}
\label{sec:iou}
The Jaccard Index or \textit{Intersection over Union} (IoU) score is a more 
robust metric to evaluate semantic segmentation tasks. It scores the overlap 
between the predicted segmentation and a ground truth mask. Equation 
\ref{eq:iou} presents two formulations of the IoU score, one based on set 
theory and the other on contingency theory. 
The IoU score penalizes especially single instances of wrong classification, 
which can also be called false positives (FP) and therefore has a ``squaring'' 
effect similar to the L2 regularization \cite{muller_towards_2022}.
\begin{equation}
J(A, B) = \frac{|A \cap B|}{|A \cup B|} = \frac{TP}{FN + FP + TP}
 \label{eq:iou}
\end{equation}

\subsection{Dice score}
\label{sec:dice}
The \textit{Dice score}, also known as Sørensen–Dice coefficient or F1 score, 
also measures the 
overlap between a predicted segmentation and a ground truth mask. As already 
pointed out in Section \ref{sec:iou}, the Dice score is more lenient in 
penalizing false classifications compared to the IoU Score. This could be due 
to the lack of information about segmentation error type, i.e. if over- or 
undersegmentation occurred \cite{popovic_statistical_2007}.
\begin{equation}
 DS(A, B) = \frac{2|A \cap B|}{|A| + |B|} = \frac{2TP}{2TP+FP+FN}
\end{equation}

\subsection{Sensitivity}
\textit{Sensitivity}, also known as recall, is a standard metric for 
performance evaluation. It evaluates the true positive detection 
capabilities of a classifier or test. It refers to a model's ability to 
correctly identify positive cases, i.e., cases that belong to the target 
class \cite{muller_towards_2022}. A high sensitivity is essential in 
healthcare, as missing positive cases can have serious consequences. Due to its 
focus on the true positives, sensitivity can only give evidence about the true 
positive rate, another name for sensitivity, and not a model's false positive 
rate \cite{akobeng_understanding_2007}.
\begin{equation}
 sensitivity = \frac{TP}{TP+FN}
 \label{eq:sensitivity}
\end{equation}
\subsection{Specificity}
\textit{Specificity} is another commonly used evaluation metric. It assesses a 
model's true negative detection rate, i.e., a 
model's ability to correctly identify negative cases that do not belong to the 
target class \cite{akobeng_understanding_2007}. In other words, specificity 
measures the proportion of true negatives out of all negative cases. In a 
binary 
segmentation case, the specificity reflects a model's ability to recognize an 
image's background class \cite{popovic_statistical_2007}. Therefore, 
specificity provides insight into how well a model can differentiate between 
the target and non-target classes.
\begin{equation}
 specificity = \frac{TN}{TN+FP}
 \label{eq:specificity}
\end{equation}
\subsection{Balanced Accuracy}
\textit{Balanced accuracy} is often used in addition to the classical 
definition of accuracy since it is more sensitive to class imbalances in a 
dataset. 
If the balanced accuracy differs significantly from the average accuracy, the 
imbalance affects the classifier regarding class prevalence. Consequently, it 
is biased against one of the outcomes of the target variable. The balanced 
accuracy is defined as the average of sensitivity and specificity 
\cite{akobeng_understanding_2007}.
\begin{equation}
 balanced \ accuracy = \frac{\frac{TP}{TP+FN}\frac{TN}{TN+FP}}{2} = 
\frac{sensitivity * specificity}{2}
 \label{eq:balanced_acc}
\end{equation}
\section{Ophthalmic Metrics}
\label{sec:ophthalmic-metrics}
In addition to the visual analysis of the fundus image, ophthalmologists also 
use certain metrics to diagnose glaucoma. Some of the most used metrics are the 
cup-to-disc ratio, the 
rim-to-disc ratio, and the ISNT rule. The following section will explain these 
three metrics as these are employed in the presented work.
\subsection{(Vertical) Cup-to-disc Ratio}
\label{sec:cdr}
The \textit{(vertical) cup-to-disc ratio} (vCDR) is defined as the ratio 
between the vertical diameter of the optic cup and disc, as shown in Figure 
\ref{fig:cdr}. Commonly, a vCDR greater than 0.5 
can be considered glaucomatous. However, the vCDR is often criticized for its 
ineffectiveness in diagnosing glaucoma, as it depends on the optic 
disc size \cite{greslechner_klinische_2016}. 
Nevertheless, the vCDR is still often used in the biomedical engineering 
community to evaluate glaucoma successfully. It is also used in this thesis.
\begin{figure}[ht]
 \centering
 \includegraphics[width=0.3\linewidth]{cdr}
 \caption{Retinal fundus image showing the optic disc (blue) and optic 
cup (yellow) with the vertical cup and disc diameter}
 \label{fig:cdr}
\end{figure}

\subsection{Rim-to-disc Ratio}
The \textit{rim-to-disc ratio} (RDR) is defined as the ratio between the width 
of the neuroretinal rim and the diameter of the optic disc. \Cref{fig:rdr} 
shows a retinal fundus image with the optic disc in blue and the optic cup in 
yellow. It also displays the cup's diameter and the neuroretinal rim's width. A 
smaller RDR commonly indicates a higher risk of glaucoma since the neuroretinal 
rim is thinning and 
less nerve tissue is functional. A large RDR is commonly associated with a 
lower risk of glaucoma \cite{henderer_disc_2006}. 
A major advantage of the RDR is that it is less affected by 
variations in the size and shape of the optic disc. The cup-to-disc ratio can be 
influenced by the size of the disc, which can vary between individuals and even 
between eyes in the same individual. In contrast, the RDR focuses 
on the thickness of the neuroretinal rim, which is less influenced by these 
variations. Therefore, the RDR can give a more comprehensive assessment of 
the optic nerve head \cite{kumar_rim--disc_2019, henderer_disc_2006}.
\begin{figure}[ht]
 \centering
 \includegraphics[width=0.3\linewidth]{rdr}
 \caption{Retinal fundus image showing the optic disc (blue) and optic 
cup (yellow) with cup diameter (green) and width of neuroretinal rim (red)}
 \label{fig:rdr}
\end{figure}
\subsection{ISNT Rule}
The \textit{ISNT} (inferior $\geq$ superior $\geq$ nasal $\geq$ temporal) rule 
is a mnemonic used in the diagnosis of glaucoma to describe the typical pattern 
of optic nerve fiber layer thickness.
The neuroretinal rim is the intra-papillary equivalent of the nerve fibers. In 
healthy individuals, the rim varies according to different quadrants. The 
inferior quadrant is the broadest, followed by the superior and nasal 
quadrants, while the temporal quadrant is the thinnest. \Cref{fig:isnt} 
illustrates the four quadrants. Deviations in this 
pattern can indicate a degradation of the retinal nerve fibers and, thereby, 
glaucoma \cite{greslechner_klinische_2016, harizman_isnt_2006}. To verify the 
ISNT rule, the thickness of the neuroretinal rim is computed for each quadrant 
and subsequently ordered based on the quadrants. Due to taking 
into account the different quadrants, it is important to know whether the ISNT 
rule is computed for the left or right eye. In contrast, the vCDR and RDR are 
side agnostic. The ISNT rule can be a helpful mnemonic for ophthalmologists to 
diagnose glaucoma. However, it should only be used in combination with other 
clinical factors. The ISNT rule is not always applicable, as 
\citeauthor{harizman_isnt_2006} found that it did not apply to 20\% of healthy 
individuals and did apply to 28\% of individuals with glaucoma 
\cite{harizman_isnt_2006}.

\begin{figure}[ht]
 \centering
 \includegraphics[width=0.3\linewidth]{isnt_right_eye}
 \caption{Retinal fundus image of the right eye showing the neuroretinal rim 
(blue) 
and optic cup (yellow) with the four quadrants of the ISNT rule highlighted}
 \label{fig:isnt}
\end{figure}
