import torch
import torch.nn as nn
import torchvision.models as models


class ResNetEmbedding(nn.Module):
    def __init__(self, nr_channels):
        super(ResNetEmbedding, self).__init__()
        resnet = models.resnet34(weights='ResNet34_Weights.IMAGENET1K_V1')
        for param in resnet.parameters():
            param.requires_grad = False

        modules = list(resnet.children())[:-1]
        self.resnet = nn.Sequential(*modules)
        self.resnet[0] = nn.Conv2d(nr_channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)

    def forward(self, image):
        features = self.resnet(image)
        features = features.view(features.size(0), -1)

        return features