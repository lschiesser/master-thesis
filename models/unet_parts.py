import torch
from torch import nn
import torch.nn.functional as F


class ConvBlock(nn.Module):
    def __init__(self, inputChannels, outputChannels):
        super(ConvBlock, self).__init__()
        self.conv1 = nn.Conv2d(inputChannels, outputChannels, kernel_size=3, padding=1, bias=False)
        self.relu = nn.ReLU()
        self.conv2 = nn.Conv2d(outputChannels, outputChannels, kernel_size=3, padding=1, bias=False)

    def forward(self, x):
        return self.relu(self.conv2(self.relu(self.conv1(x))))


class DownBlock(nn.Module):
    def __init__(self, inputChannels, outputChannels):
        super(DownBlock, self).__init__()
        self.maxpool = nn.MaxPool2d(2)
        self.ConvBlock = ConvBlock(inputChannels, outputChannels)

    def forward(self, x):
        x = self.maxpool(x)
        x = self.ConvBlock(x)
        return x


class UpBlock(nn.Module):
    def __init__(self, inputChannels, outputChannels, bilinear=False):
        super(UpBlock, self).__init__()
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = ConvBlock(inputChannels, outputChannels)
        else:
            self.up = nn.ConvTranspose2d(inputChannels, inputChannels // 2, kernel_size=2, stride=2)
            self.conv = ConvBlock(inputChannels, outputChannels)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, (diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2))
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, inputChannels, outputChannels, sigmoid=False):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(inputChannels, outputChannels, kernel_size=1)
        self.sigmoid = sigmoid
        if sigmoid:
            self.sigmoid = nn.Sigmoid()
    def forward(self, x):
        if self.sigmoid:
            return self.sigmoid(self.conv(x))
        return self.conv(x)
