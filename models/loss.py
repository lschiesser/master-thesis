import torch.nn.functional as F
import torch


def dice_loss(output, target, smooth=1):
    output = output.view(-1)
    target = target.view(-1)

    intersection = (output * target).sum()
    dice = (2. * intersection + smooth) / (output.sum() + target.sum() + smooth)

    return 1 - dice


def focal_loss(output, target, alpha=0.8, gamma=2, reduction="none"):
    output = output.view(-1)
    target = target.view(-1)

    CE = F.cross_entropy(output, target, reduction=reduction)  # = -log(pt)
    CE_EXP = torch.exp(-CE)  # = pt
    focal_loss = -alpha * (1 - CE_EXP) ** gamma * -CE  # = -alpha * (1 - pt) ** gamma * log(pt)
    return focal_loss


def crossentropy_loss(output, target):
    output = output.squeeze()
    target = target.squeeze()
    return F.cross_entropy(output, target)
