from models.unet_parts import ConvBlock, DownBlock, UpBlock, OutConv
import torch.nn as nn


class UNet(nn.Module):
    def __init__(self, n_channels, n_classes, sigmoid=False):
        super(UNet, self).__init__()
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.sigmoid = sigmoid

        self.inBlock = ConvBlock(self.n_channels, 64)
        self.down1 = DownBlock(64, 128)
        self.down2 = DownBlock(128, 256)
        self.down3 = DownBlock(256, 512)
        self.down4 = DownBlock(512, 1024)
        self.up1 = UpBlock(1024, 512)
        self.up2 = UpBlock(512, 256)
        self.up3 = UpBlock(256, 128)
        self.up4 = UpBlock(128, 64)
        self.outBlock = OutConv(64, self.n_classes, self.sigmoid)

    def forward(self, x):
        x1 = self.inBlock(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        x = self.outBlock(x)
        return x
