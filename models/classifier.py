import torch
import torch.nn as nn
import torchvision.models as models



class GlaucomaClassifier(nn.Module):

    def __init__(self, nr_numeric_features, nr_channels=3):
        super(GlaucomaClassifier, self).__init__()
        self.resnet = models.resnet34(weights='ResNet34_Weights.IMAGENET1K_V1')
        for param in self.resnet.parameters():
            param.requires_grad = False

        self.resnet.fc = nn.Identity()
        self.resnet.conv1 = nn.Conv2d(nr_channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)

        self.linear_relu_stack = nn.Sequential(
            nn.Linear(nr_numeric_features, 64),
            nn.ReLU(),
            nn.Linear(64, 128),
            nn.ReLU(),
            nn.Linear(128, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU()
        )
        self.output = nn.Sequential(
            nn.Linear(512+64, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            nn.Sigmoid()
        )


    def forward(self, image_input, metric_input):
        img = self.resnet(image_input)
        metrics = self.linear_relu_stack(metric_input)
        i = torch.cat((img, metrics), -1)
        logits = self.output(i)
        return logits

class GlaucomaResnet(nn.Module):

    def __init__(self, nr_channels=3):
        super(GlaucomaResnet, self).__init__()
        self.resnet = models.resnet34(weights='ResNet34_Weights.IMAGENET1K_V1')

        for param in self.resnet.parameters():
            param.requires_grad = False

        self.resnet.fc = nn.Identity()
        self.resnet.conv1 = nn.Conv2d(nr_channels, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)

        self.output = nn.Sequential(
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            #nn.Sigmoid()
        )

    def forward(self, image_input, metrics):
        img = self.resnet(image_input)
        logits = self.output(img)
        return logits


class EmbedSegClassifier(nn.Module):
    def __init__(self):
        super(EmbedSegClassifier, self).__init__()
        self.classifier = nn.Sequential(
            nn.Linear(512 * 3, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 1)
        )

    def forward(self, input):
        logits = self.classifier(input)
        return logits

class EmbedImgClassifier(nn.Module):
    def __init__(self):
        super(EmbedImgClassifier, self).__init__()
        self.classifier = nn.Sequential(
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 1)
        )

    def forward(self, img_emebdding):
        logits = self.classifier(img_emebdding)
        return logits

class EmbedImgMetClassifier(nn.Module):
    def __init__(self, nr_numeric_features):
        super(EmbedImgMetClassifier, self).__init__()
        self.embed_classifier = nn.Sequential(
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 128)
        )
        self.metric_classifier = nn.Sequential(
            nn.Linear(nr_numeric_features, 64),
            nn.ReLU(),
            nn.Linear(64, 128),
            nn.ReLU(),
            nn.Linear(128, 256),
            nn.ReLU(),
            nn.Linear(256, 128)
        )
        self.output = nn.Sequential(
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 1)
        )
    def forward(self, img_emebdding, numeric_input):
        embeds = self.embed_classifier(img_emebdding)
        metrics = self.metric_classifier(numeric_input)
        i = torch.cat((embeds, metrics), -1)
        logits = self.output(i)
        return logits


class EmbedSegMetClassifier(nn.Module):
    def __init__(self, nr_numeric_features):
        super(EmbedSegMetClassifier, self).__init__()
        self.embed_classifier = nn.Sequential(
            nn.Linear(512 * 3, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 128)
        )
        self.metric_classifier = nn.Sequential(
            nn.Linear(nr_numeric_features, 64),
            nn.ReLU(),
            nn.Linear(64, 128),
            nn.ReLU(),
            nn.Linear(128, 256),
            nn.ReLU(),
            nn.Linear(256, 128)
        )
        self.output = nn.Sequential(
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 1)
        )
    def forward(self, img_emebdding, numeric_input):
        embeds = self.embed_classifier(img_emebdding)
        metrics = self.metric_classifier(numeric_input)
        i = torch.cat((embeds, metrics), -1)
        logits = self.output(i)
        return logits