import torch
import segmentation_models_pytorch as smp
from matplotlib.colors import ListedColormap

from models.classifier import GlaucomaClassifier
import numpy as np
import albumentations as A
from collections import OrderedDict
import argparse
import random
from PIL import Image
from util.utils import load_model
import torchvision.transforms as F
from util.utils import compute_metrics
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Prediction Script")
parser.add_argument('-p', '--path', type=int, help="path to image")

args = parser.parse_args()

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

transform = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])

to_tensor = F.ToTensor()

# load models
seg_model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=3
)

seg_state_dict = load_model('outputs/segmentation/best_model.pth')
seg_model.load_state_dict(seg_state_dict)
seg_model.to(device)
seg_model.eval()

bloodvessel_model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=2
)

bloodvessel_state_dict = load_model('outputs/bloodvessel/best_model.pth')
bloodvessel_model.load_state_dict(bloodvessel_state_dict)
bloodvessel_model.to(device)
bloodvessel_model.eval()

nr_features = 1
classifier = GlaucomaClassifier(nr_numeric_features=nr_features)

classifier_state_dict = load_model('outputs/classifier/best_model.pth')
classifier.load_state_dict(classifier_state_dict)
classifier.to(device)
classifier.eval()
# load image
img = Image.open(args.path).convert("RGB").resize((512, 512))
img_tmp = to_tensor(img).unsqueeze(0).to(device)
# 1) segment bloodvessels
vessels = bloodvessel_model(img_tmp)
vessels = torch.argmax(vessels, 1)
# 1) segment cup and disc
biomarkers = seg_model(img)
biomarkers = torch.argmax(vessels, 1)
transformed = transform(image=img)
img = transformed['image']
# 2) create new tensor
image = torch.cat((img, biomarkers, vessels), 0)

# 3) compute metrics
metrics = compute_metrics(biomarkers)

# 4) perform classification
pred = classifier(image, metrics)
pred = torch.round(pred)

# display pictures, metrics and classification
fig, axes = plt.subplots(1, 3)
axes[0].imshow(img)
axes[1].imshow(img)
axes[1].imshow(biomarkers, cmap=ListedColormap(['#0099ff50', '#99ff9950', '#ffffff00']))
axes[2].imshow(img)
axes[2].imshow(vessels, cmap=ListedColormap(['#ffffff00', 'white']))
# add metrics
fig.tight_layout()
fig.show()
