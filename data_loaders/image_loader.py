import torch
from torch.utils import data
from util.rgb_class import map_rgb_to_indices
from PIL import Image
import numpy as np
import torchvision.transforms as T
import matplotlib.pyplot as plt
from occular_metrics import metrics


class SegmentationDataSet(data.Dataset):
    def __init__(self,
                 images,
                 targets,
                 transforms=None):
        self.images = images
        self.targets = targets
        self.transforms = transforms
        self.dtype = torch.float32
        self.to_tensor = T.ToTensor()

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index: int):
        input_ID = self.images[index]
        target_ID = self.targets[index]

        # Load input and target
        x, y = np.array(Image.open(input_ID).convert('RGB')), np.array(Image.open(target_ID))

        glaukom = False
        if "glaukom" in input_ID:
            glaukom = True

        # Preprocessing
        if self.transforms is not None:
            transformed = self.transforms(image=x, mask=y)
            x = transformed['image']
            y = transformed['mask']

        # Typecasting
        x = self.to_tensor(x)
        y = map_rgb_to_indices(y).type(self.dtype)
        return x, y, glaukom

    def visualize(self, index):
        origImage, predMask, _ = self[index]
        figure, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 15))

        origImage = np.transpose(origImage.squeeze(0), (1, 2, 0))
        predMask = predMask.squeeze(0)
        print(predMask.shape)
        ax[0].imshow(origImage)
        ax[1].imshow(predMask)
        figure.tight_layout()
        figure.show()


class ClassificationDataSet(data.Dataset):
    def __init__(self, images, biomarker, vessels=[], segmentations=False, metrics=False, transforms=None):
        self.images = images
        self.biomarker = biomarker
        self.vessels = vessels
        self.transforms = transforms
        self.dtype = torch.float32
        self.to_tensor = T.ToTensor()
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.segmentations = segmentations
        self.metrics = metrics

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index: int):
        # load image and mask
        input_ID = self.images[index]
        marker = self.biomarker[index]
        image, marker = np.array(Image.open(input_ID).convert('RGB').resize((512, 512))), np.array(Image.open(marker))

        image = self.to_tensor(image)

        marker = map_rgb_to_indices(marker).type(self.dtype)
        marker = marker/2

        glaukom = 0
        if "glaukom" in input_ID:
            glaukom = 1

        if self.segmentations:

            vessel = self.vessels[index]

            # Load input and target
            vessel = np.array(Image.open(vessel))
            vessel = self.to_tensor(vessel)

            marker = marker.unsqueeze(0)
            image = torch.cat((marker, vessel, image), 0)

        ophthalmic_metrics = torch.Tensor([])
        if self.metrics:
            ophthalmic_metrics = self.compute_metrics(marker)


        glaukom = torch.tensor([glaukom], dtype=self.dtype)
        return image, ophthalmic_metrics, glaukom

    def compute_metrics(self, mask):
        cdr = metrics.compute_cup_disk_ratio(mask)
        isnt = metrics.compute_isnt(mask)
        # bloodvessels = metrics.compute_bloodvessels_isnt(mask, vessels)
        # ophthalmic_metrics = [cdr]
        ophthalmic_metrics = [[cdr], isnt] #, [x[1] for x in bloodvessels]]
        ophthalmic_metrics = [item for sublist in ophthalmic_metrics for item in sublist]
        ophthalmic_metrics = torch.tensor(ophthalmic_metrics, dtype=self.dtype)
        return ophthalmic_metrics


