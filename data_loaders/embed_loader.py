import torch
from torch.utils import data
from util.rgb_class import map_rgb_to_indices
from PIL import Image
import numpy as np
from occular_metrics import metrics as ometrics


class EmbedLoader(data.Dataset):
    def __init__(self, images, biomarker, vessels=[], segmentations=False, metrics=False, isnt=False):
        self.images = images
        self.biomarker = biomarker
        self.vessels = vessels
        self.segmentations = segmentations
        self.metrics = metrics
        self.isnt = isnt


    def __len__(self):
        return len(self.images)

    def __getitem__(self, item):
        image_path = self.images[item]
        marker_path = self.biomarker[item]
        image = torch.load(image_path)

        glaukom = 0
        if "glaukom" in image_path:
            glaukom = 1

        if self.segmentations:
            vessel_path = self.vessels[item]
            marker = torch.load(marker_path)
            vessel = torch.load(vessel_path)

            image = torch.cat((image, marker, vessel), -1)
        image = image.squeeze()

        glaukom = torch.tensor([glaukom], dtype=torch.float32)

        if self.metrics:
            metrics = self.compute_metrics(marker_path, self.isnt)
            return image, metrics, glaukom

        return image, glaukom

    def compute_metrics(self, marker_path, isnt):
        marker_path = marker_path.replace("EmbedSALUS", "AugSALUS").replace('.pt', '.png')
        marker = np.array(Image.open(marker_path))
        marker = map_rgb_to_indices(marker)
        cdr = ometrics.compute_cup_disk_ratio(marker)
        rdr = ometrics.compute_rim_disc_ratio(marker)
        metrics = [cdr, rdr]
        if isnt:
            isnt = ometrics.compute_isnt(marker)
            metrics = [[cdr], [rdr], isnt]
            metrics = [item for sublist in metrics for item in sublist]
        metrics = torch.tensor(metrics, dtype=torch.float32)
        return metrics


