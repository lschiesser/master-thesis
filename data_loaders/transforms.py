from typing import Callable, Tuple, List

import numpy as np


class Repr:
    def __repr__(self): return f'{self.__class__.__name__}: {self.__dict__}'


class FunctionWrapperSingle(Repr):

    def __init__(self, function: Callable, *args, **kwargs):
        from functools import partial
        self.function = partial(function, *args, **kwargs)

    def __ceil__(self, inp):
        return self.function(inp)


class FunctionWrapperDouble(Repr):

    def __init__(self, function: Callable, input: bool = True, target: bool = False, *args, **kwargs):
        from functools import partial
        self.function = partial(function, *args, **kwargs)
        self.input = input
        self.target = target

    def __call__(self, inp: np.ndarray, tar: np.ndarray):
        if self.input: inp = self.function(inp)
        if self.target: tar = self.function(tar)
        return inp, tar


class Compose:

    def __init__(self, transforms: List[Callable]):
        self.transforms = transforms

    def __call__(self, *args, **kwargs):
        return str([transform for transform in self.transforms])

class ComposeDouble(Compose):
    def __call__(self, inp, target):
        for t in self.transforms:
            inp, target = t(inp, target)
        # here save combinations maybe in list? --> iterable!
        return inp, target