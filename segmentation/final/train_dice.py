import torch
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
from data_loaders.image_loader import SegmentationDataSet
from util.save_best_model import SaveBestModel, save_model
from util.utils import get_filenames_of_path
import albumentations as A
import segmentation_models_pytorch as smp
import random
from tqdm import tqdm
import argparse
import matplotlib.pyplot as plt
import json

parser = argparse.ArgumentParser(description="Train Segmentation")
parser.add_argument('-e', '--epochs', type=int, help="Number of epochs", default=10)

print("Dice loss final config")

args = parser.parse_args()

BATCH_SIZE = 25
LEARNING_RATE = 1e-3
EPOCHS = 40
random_seed = 42
training_size = 0.8

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

torch.manual_seed(random_seed)
random.seed(random_seed)

images = get_filenames_of_path('datasets/SALUS/train/glaukom/images/', '*.png') + get_filenames_of_path('datasets/SALUS/train/normal/images/', '*.png')
masks = get_filenames_of_path('datasets/SALUS/train/glaukom/masks/', '*.png') + get_filenames_of_path('datasets/SALUS/train/normal/masks/', '*.png')

images_train, images_valid = train_test_split(
    images,
    random_state=random_seed,
    train_size=training_size,
    shuffle=True
)

targets_train, targets_valid = train_test_split(
    masks,
    random_state=random_seed,
    train_size=training_size,
    shuffle=True
)

transforms_train = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=1),
    A.Rotate(limit=10, p=1)
])

transforms_train2 = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=.6),
    A.Rotate(limit=10, p=.6)
])

augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])

print("Loading images...")
dataset_train1 = SegmentationDataSet(images=images_train, targets=targets_train, transforms=transforms_train)
dataset_train2 = SegmentationDataSet(images=images_train, targets=targets_train, transforms=augs_resize)
dataset_train3 = SegmentationDataSet(images=images_train, targets=targets_train, transforms=transforms_train2)
dataset_train = torch.utils.data.ConcatDataset([dataset_train1, dataset_train2, dataset_train3])

dataset_valid = SegmentationDataSet(images=images_valid, targets=targets_valid, transforms=augs_resize)


dataloader_train = DataLoader(dataset_train, batch_size=BATCH_SIZE, shuffle=True)
dataloader_valid = DataLoader(dataset_valid, batch_size=1, shuffle=True)
print("Images loaded!")
loss_fn = smp.losses.DiceLoss(mode='multiclass').to(device)

model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=3
).to(device)
model = torch.nn.DataParallel(model)
model.train(True)

optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

save_best_model = SaveBestModel()

S = {"train_loss": [], "test_loss": [], "test_iou": [], "test_acc": [], "test_dice": [], "dice_cup": [], "dice_disc":[],
     "iou_cup": [], "iou_disc": []}

trainSteps = len(dataloader_train)
testSteps = len(dataloader_valid)
print("Training model...")
for e in tqdm(range(EPOCHS)):
    model.train()
    totalTrainLoss = 0
    totalTestLoss = 0
    totalTestAccuracy = 0
    totalTestIOU = 0
    totalTestDice = 0
    valid_loss = 0
    for batch, (X, y, label) in enumerate(dataloader_train):
        X, y = X.to(device), y.to(device).long()
        optimizer.zero_grad()
        pred = model(X)
        loss = loss_fn(pred, y)

        totalTrainLoss += loss.item()

        loss.backward()
        optimizer.step()

    tp = {'0': 0, '1': 0, '2': 0}
    tn = {'0': 0, '1': 0, '2': 0}
    fp = {'0': 0, '1': 0, '2': 0}
    fn = {'0': 0, '1': 0, '2': 0}

    with torch.no_grad():
        model.eval()
        for X, y, label in dataloader_valid:
            X, y = X.to(device), y.to(device).long()
            pred = model(X)
            valid_loss = loss_fn(pred, y)
            pred = torch.argmax(pred, dim=1)
            metrics = smp.metrics.get_stats(pred, y, mode="multiclass", num_classes=3)
            iou_score = smp.metrics.iou_score(*metrics, reduction="micro")
            dice_score = smp.metrics.f1_score(*metrics, reduction="micro")
            accuracy = smp.metrics.accuracy(*metrics, reduction="macro")
            totalTestAccuracy += accuracy.item()
            totalTestIOU += iou_score.item()
            totalTestDice += dice_score.item()
            totalTestLoss += valid_loss.item()
            stats_tensors = []
            stats = {0: "tp", 1: "fp", 2: "fn", 3: "tn"}
            for idx, tensor in enumerate(metrics):
                # Sum the columns in each tensor
                reduced = torch.sum(tensor, dim=-2).numpy()
                for i, val in enumerate(reduced):
                    if i in torch.unique(y):
                        if idx == 0:
                            tp[str(i)] += val
                        elif idx == 1:
                            fp[str(i)] += val
                        elif idx == 2:
                            fn[str(i)] += val
                        elif idx == 3:
                            tn[str(i)] += val
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()
    avg_train_loss = totalTrainLoss / trainSteps
    avg_test_loss = totalTestLoss / testSteps
    avg_test_acc = totalTestAccuracy / testSteps
    avg_test_iou = totalTestIOU / testSteps
    avg_test_dice = totalTestDice / testSteps
    S['test_iou'].append(avg_test_iou)
    S['test_acc'].append(avg_test_acc)
    S['test_dice'].append(avg_test_dice)
    S['train_loss'].append(avg_train_loss)
    S['test_loss'].append(avg_test_loss)
    S['dice_disc'].append(smp.metrics.f1_score(torch.tensor([tp['0']]), torch.tensor([fp['0']]),
                                                      torch.tensor([fn['0']]), torch.tensor([tn['0']]),
                                                      reduction="micro").item())
    S['dice_cup'].append(smp.metrics.f1_score(torch.tensor([tp['1']]), torch.tensor([fp['1']]),
                                                     torch.tensor([fn['1']]), torch.tensor([tn['1']]),
                                                     reduction="micro").item())
    S['iou_disc'].append(smp.metrics.iou_score(torch.tensor([tp['0']]), torch.tensor([fp['0']]),
                                                      torch.tensor([fn['0']]), torch.tensor([tn['0']]),
                                                      reduction="micro").item())
    S['iou_cup'].append(smp.metrics.iou_score(torch.tensor([tp['1']]), torch.tensor([fp['1']]),
                                                     torch.tensor([fn['1']]), torch.tensor([tn['1']]),
                                                     reduction="micro").item())


    print(f"EPOCH: {e+1}/{EPOCHS}")
    print("Train loss: {:.6f}, Validation loss: {:.6f}".format(
        avg_train_loss, avg_test_loss
    ))
    save_best_model(avg_test_loss, e, model, optimizer, loss_fn, 'outputs/segmentation/dice/')
save_model(EPOCHS, model, optimizer, loss_fn, 'outputs/segmentation/dice/')
print("Done!")

plt.style.use("ggplot")
plt.figure()
plt.plot(S['train_loss'], label="Train loss")
plt.plot(S['test_loss'], label="Validation loss")
plt.title("Training Loss on Segmentation Dataset")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend(loc="lower left")
plt.savefig('graphs/dice_segmentation_train_loss.png')

# Graph for IOU score and accuracy
plt.style.use("ggplot")
plt.figure()
plt.plot(S['test_iou'], label="Validation IOU")
plt.plot(S['test_acc'], label="Validation accuracy")
plt.plot(S['test_dice'], label="Validation dice")
plt.title("Validation metrics on Segmentation Dataset")
plt.xlabel("Epochs")
plt.ylabel("%")
plt.legend(loc="lower left")
plt.savefig('graphs/dice_segmentation_train_metrics.png')
# model output as 4th channel then fed to Resnet or similar?
with open("outputs/segmentation/dice/metrics.json", "w") as outfile:
    json.dump(S, outfile)
