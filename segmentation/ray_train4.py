import torch
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split
from data_loaders.image_loader import SegmentationDataSet
from util.save_best_model import SaveBestModel, save_model
from util.utils import get_filenames_of_path
import albumentations as A
import segmentation_models_pytorch as smp
import random
from tqdm import tqdm
import argparse
import numpy as np
from ray import tune
import os

parser = argparse.ArgumentParser(description="Train Segmentation")
parser.add_argument('-e', '--epochs', type=int, help="Number of epochs", default=10)

args = parser.parse_args()

BATCH_SIZE = 1
LEARNING_RATE = 1e-3
EPOCHS = args.epochs
random_seed = 42
training_size = 0.8

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

torch.manual_seed(random_seed)
random.seed(random_seed)

ROOT_DIR = os.path.abspath('./datasets/SALUS/')
images = get_filenames_of_path(ROOT_DIR+'/train/glaukom/images/', '*.png') + get_filenames_of_path(ROOT_DIR+'/train/normal/images/', '*.png')
masks = get_filenames_of_path(ROOT_DIR+'/train/glaukom/masks/', '*.png') + get_filenames_of_path(ROOT_DIR+'/train/normal/masks/', '*.png')

images_train, images_valid = train_test_split(
    images,
    random_state=random_seed,
    train_size=training_size,
    shuffle=True
)

targets_train, targets_valid = train_test_split(
    masks,
    random_state=random_seed,
    train_size=training_size,
    shuffle=True
)

transforms_train = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=1),
    A.Rotate(limit=10, p=1)
])

transforms_train2 = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512),
    A.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0, p=.6),
    A.Rotate(limit=10, p=.6)
])

augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])
print("Loading images...")
dataset_train1 = SegmentationDataSet(images=images_train, targets=targets_train, transforms=transforms_train)
dataset_train2 = SegmentationDataSet(images=images_train, targets=targets_train, transforms=augs_resize)
dataset_train3 = SegmentationDataSet(images=images_train, targets=targets_train, transforms=transforms_train2)
dataset_train = torch.utils.data.ConcatDataset([dataset_train1, dataset_train2, dataset_train3])

dataset_valid = SegmentationDataSet(images=images_valid, targets=targets_valid, transforms=augs_resize)



def train_focal_net(config, checkpoint_dir=None):
    BATCH_SIZE = config['batch']
    #os.chdir('C:/eigene_dateien/ukm/torch')
    dataloader_train = DataLoader(dataset_train, batch_size=BATCH_SIZE, shuffle=True)
    dataloader_valid = DataLoader(dataset_valid, batch_size=BATCH_SIZE, shuffle=True)
    print("Images loaded!")
    loss_fn = torch.hub.load(
        'adeelh/pytorch-multi-class-focal-loss',
        model='focal_loss',
        # alpha=torch.tensor([3, 12, 3, 4, 1]),
        alpha=torch.tensor(config['alpha']),
        gamma=2,
        device=device,
        reduction='mean',
        force_reload=False
    )
    model = smp.Unet(
        encoder_name='resnet34',
        encoder_weights='imagenet',
        decoder_use_batchnorm=True,
        classes=3
    ).to(device)
    model = torch.nn.DataParallel(model)
    model.train(True)

    optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

    save_best_model = SaveBestModel()

    S = {"train_loss": [], "test_loss": [], "test_iou": [], "test_acc": [], "test_dice": []}

    trainSteps = len(dataloader_train)
    testSteps = len(dataloader_valid)
    print("Training model...")
    for e in tqdm(range(config['epochs'])):
        model.train()
        totalTrainLoss = 0
        totalTestLoss = 0
        totalTestAccuracy = 0
        totalTestIOU = 0
        totalTestDice = 0
        valid_loss = 0
        for batch, (X, y, label) in enumerate(dataloader_train):
            X, y = X.to(device), y.to(device).long()
            optimizer.zero_grad()
            pred = model(X)
            loss = loss_fn(pred, y)

            totalTrainLoss += loss.item()

            loss.backward()
            optimizer.step()

        with torch.no_grad():
            model.eval()
            for X, y, label in dataloader_valid:
                X, y = X.to(device), y.to(device).long()
                pred = model(X)
                valid_loss = loss_fn(pred, y)
                pred = torch.argmax(pred, dim=1)
                tp, fp, fn, tn = smp.metrics.get_stats(pred, y, mode="multiclass", num_classes=3)
                iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="micro")
                dice_score = smp.metrics.f1_score(tp, fp, fn, tn, reduction="micro")
                accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro")
                totalTestAccuracy += accuracy
                totalTestIOU += iou_score
                totalTestDice += dice_score
                totalTestLoss += valid_loss.item()
                # correct += (pred.argmax(1) == y).type(torch.float).sum().item()
        avg_train_loss = totalTrainLoss / trainSteps
        avg_test_loss = totalTestLoss / testSteps
        avg_test_acc = totalTestAccuracy / testSteps
        avg_test_iou = totalTestIOU / testSteps
        avg_test_dice = totalTestDice / testSteps
        S['test_iou'].append(avg_test_iou)
        S['test_acc'].append(avg_test_acc)
        S['test_dice'].append(avg_test_dice)
        S['train_loss'].append(avg_train_loss)
        S['test_loss'].append(avg_test_loss)
        tune.report(mean_accuracy=avg_test_acc, mean_iou=avg_test_iou, mean_dice=avg_test_dice, val_loss=avg_test_loss, train_loss=avg_train_loss)
        print(f"EPOCH: {e+1}/{config['epochs']}")
        print("Train loss: {:.6f}, Validation loss: {:.6f}".format(
            avg_train_loss, avg_test_loss
        ))
        #save_best_model(avg_test_loss, e, model, optimizer, loss_fn, 'outputs/segmentation/')
    #save_model(EPOCHS, model, optimizer, loss_fn, 'outputs/segmentation/')
    print("Done!")

    # plt.style.use("ggplot")
    # plt.figure()
    # plt.plot(S['train_loss'], label="Train loss")
    # plt.plot(S['test_loss'], label="Validation loss")
    # plt.title("Training Loss on Segmentation Dataset")
    # plt.xlabel("Epochs")
    # plt.ylabel("Loss")
    # plt.legend(loc="lower left")
    # plt.savefig('graphs/segmentation_train_loss.png')

    # Graph for IOU score and accuracy
    # plt.style.use("ggplot")
    # plt.figure()
    # plt.plot(S['test_iou'], label="Validation IOU")
    # plt.plot(S['test_acc'], label="Validation accuracy")
    # plt.plt(S['test_dice'], label="Validation dice")
    # plt.title("Validation metrics on Segmentation Dataset")
    # plt.xlabel("Epochs")
    # plt.ylabel("%")
    # plt.legend(loc="lower left")
    # plt.savefig('graphs/segmentation_train_metrics.png')

search_space = {
    "batch": tune.grid_search([2, 4, 10, 20, 25]),
    "alpha": tune.grid_search([
        # [0=disk, 1=cup, 2=bg]
        [1/0.28, 1/0.1, 1/0.62],
        [1/0.3, 1/0.1, 1/0.6],
        [0.7, 0.9, 0.4],
    ]),
    'epochs': tune.grid_search([60])
}

reporter = tune.CLIReporter(
        # parameter_columns=["l1", "l2", "lr", "batch_size"],
        metric_columns=["val_loss", "mean_accuracy", "mean_iou", "training_iteration"])

scheduler = tune.schedulers.ASHAScheduler(
    metric="mean_iou",
    mode="max",
    grace_period=10
)

result = tune.run(
    train_focal_net,
    resources_per_trial={"cpu": 4, "gpu": 1},
    config=search_space,
    local_dir='./results',
    scheduler=scheduler,
    progress_reporter=reporter
)

best_trial = result.get_best_trial(
    'mean_iou', 'max', 'last'
)
print(f"Best trial config: {best_trial.config}")
print(f"Best trial final validation loss: {best_trial.last_result['val_loss']}")
print(f"Best trial final validation iou: {best_trial.last_result['mean_iou']}")

