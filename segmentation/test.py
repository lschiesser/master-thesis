import torch
from torch.utils.data import DataLoader
from data_loaders.image_loader import SegmentationDataSet
from util.utils import get_filenames_of_path
import segmentation_models_pytorch as smp
import numpy as np
import albumentations as A
from collections import OrderedDict
import json

BATCH_SIZE = 1
random_seed = 42

torch.manual_seed(random_seed)

loss_type = "tversky"

print("Seg model test " + loss_type)

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

images = get_filenames_of_path("datasets/SALUS/test/normal/images/", '*.png') + get_filenames_of_path("datasets/SALUS/test/glaukom/images/", '*.png')
targets = get_filenames_of_path("datasets/SALUS/test/normal/masks/", '*.png') + get_filenames_of_path("datasets/SALUS/test/glaukom/masks/", '*.png')

augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])

dataset_test = SegmentationDataSet(images=images, targets=targets, transforms=augs_resize)

dataloader_test = DataLoader(dataset_test, batch_size=BATCH_SIZE, shuffle=True)

model = smp.Unet(
    encoder_name='resnet34',
    encoder_weights='imagenet',
    decoder_use_batchnorm=True,
    classes=3
)

checkpoint = torch.load('outputs/segmentation/{}/best_model.pth'.format(loss_type), map_location=torch.device("cpu"))
new_state_dict = OrderedDict()
for k, v in checkpoint.state_dict().items():
    if k.find(".fc.") == -1:
        if k[:7] == 'module.':
            name = k[7:]  # remove module.
        else:
            name = k
        new_state_dict[name] = v
model.load_state_dict(new_state_dict)
model.to(device)
model.eval()

total_metrics = {
    'iou_score': 0,
    'accuracy': 0,
    'dice_score': 0,
    'iou_disc': 0,
    'iou_cup': 0,
    'dice_disc': 0,
    "dic_cup": 0,
}
tp = {'0': 0, '1': 0, '2': 0}
tn = {'0': 0, '1': 0, '2': 0}
fp = {'0': 0, '1': 0, '2': 0}
fn = {'0': 0, '1': 0, '2': 0}

with torch.no_grad():
    for batch, (X, y, label) in enumerate(dataloader_test):
        X, y = X.to(device), y.to(device).long()

        pred = model(X)
        pred = pred.long()

        pred = torch.argmax(pred, 1)

        metrics = smp.metrics.get_stats(pred, y, mode="multiclass", num_classes=3)
        iou_score = smp.metrics.iou_score(*metrics, reduction="micro")
        dice_score = smp.metrics.f1_score(*metrics, reduction="micro")
        accuracy = smp.metrics.accuracy(*metrics, reduction="macro")
        total_metrics['iou_score'] += iou_score.item()
        total_metrics['dice_score'] += dice_score.item()
        total_metrics['accuracy'] += accuracy.item()
        stats_tensors = []
        stats = {0: "tp", 1: "fp", 2: "fn", 3: "tn"}
        for idx, tensor in enumerate(metrics):
            # Sum the columns in each tensor
            reduced = torch.sum(tensor, dim=-2).numpy()
            for i, val in enumerate(reduced):
                if i in torch.unique(y):
                    if idx == 0:
                        tp[str(i)] += val
                    elif idx == 1:
                        fp[str(i)] += val
                    elif idx == 2:
                        fn[str(i)] += val
                    elif idx == 3:
                        tn[str(i)] += val
        # correct += (pred.argmax(1) == y).type(
n_samples = len(dataloader_test)
total_metrics['dice_disc'] = smp.metrics.f1_score(torch.tensor([tp['0']]), torch.tensor([fp['0']]), torch.tensor([fn['0']]), torch.tensor([tn['0']]), reduction="micro").item()
total_metrics['dice_cup'] = smp.metrics.f1_score(torch.tensor([tp['1']]), torch.tensor([fp['1']]), torch.tensor([fn['1']]), torch.tensor([tn['1']]), reduction="micro").item()
total_metrics['iou_disc'] = smp.metrics.iou_score(torch.tensor([tp['0']]), torch.tensor([fp['0']]), torch.tensor([fn['0']]), torch.tensor([tn['0']]), reduction="micro").item()
total_metrics['iou_cup'] = smp.metrics.iou_score(torch.tensor([tp['1']]), torch.tensor([fp['1']]), torch.tensor([fn['1']]), torch.tensor([tn['1']]), reduction="micro").item()

print("IOU Score: {:.3f} Accuracy: {:.3f}". format(
    total_metrics['iou_score'] / n_samples, total_metrics['dice_score'] / n_samples, total_metrics['accuracy'] / n_samples
))

total_metrics['iou_score'] = total_metrics['iou_score']/n_samples
total_metrics['dice_score'] = total_metrics['dice_score']/n_samples
total_metrics['accuracy'] = total_metrics['accuracy']/n_samples

with open("outputs/segmentation/{}/test_metrics.json".format(loss_type ), "w") as outfile:
    json.dump(total_metrics, outfile)

