import torch
from torch.utils.data import DataLoader
from data_loaders.image_loader import ClassificationDataSet
from util.utils import get_filenames_of_path
from models.classifier import GlaucomaClassifier
import numpy as np
import albumentations as A
from collections import OrderedDict
import random
import itertools
from sklearn.metrics import confusion_matrix

random_seed = 42
BATCH_SIZE = 1

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

torch.manual_seed(random_seed)
random.seed(random_seed)

images = get_filenames_of_path('datasets/SALUS/test/glaukom/images/', '*.png') + get_filenames_of_path('datasets/SALUS/test/normal/images/', '*.png')
masks = get_filenames_of_path('datasets/SALUS/test/glaukom/masks/', '*.png') + get_filenames_of_path('datasets/SALUS/test/normal/masks/', '*.png')

augs_resize = A.Compose([
    A.CLAHE(p=1),
    A.Resize(512, 512)
])

dataset_test = ClassificationDataSet(images=images, targets=masks, transforms=augs_resize)

dataloader_test = DataLoader(dataset_test, batch_size=BATCH_SIZE, shuffle=True)

loss_fn = torch.nn.BCELoss()
nr_features = 1
model = GlaucomaClassifier(nr_numeric_features=nr_features).to(device)

new_state_dict = OrderedDict()
checkpoint = torch.load('outputs/segmentation/best_model.pth', map_location=torch.device("cpu"))
new_state_dict = OrderedDict()
for k, v in checkpoint.state_dict().items():
    if k.find(".fc.") == -1:
        if k[:7] == 'module.':
            name = k[7:]  # remove module.
        else:
            name = k
        new_state_dict[name] = v
model.load_state_dict(new_state_dict)
model.to(device)
model.eval()

y_true = []
y_pred = []
totalTestLoss = 0
steps = len(dataloader_test)
with torch.no_grad():
    for X, metrics, y in dataloader_test:
        X, metrics, y = X.to(device), metrics.to(device), y.to(device)
        pred = model(X)
        loss = loss_fn(pred, y)
        pred = torch.round(pred)
        y_pred.append(pred.item())
        y_true.append(y.item())
        totalTestLoss += loss.item()

y_pred = [a.tolist() for a in y_pred]
y_pred = list(itertools.chain.from_iterable(y_pred))
y_true = [a.tolist() for a in y_true]
y_true = list(itertools.chain.from_iterable(y_true))
tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()
avg_loss = totalTestLoss / steps
accuracy = (tp + tn) / (tp + fp + fn + tp)
sensitivity = tp / (tp + fn)
specificity = tn / (tn + fp)





