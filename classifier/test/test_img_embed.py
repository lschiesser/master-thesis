import json
import torch
from torch.utils.data import DataLoader
from sklearn.metrics import confusion_matrix
from data_loaders.embed_loader import EmbedLoader
from util.utils import get_filenames_of_path
import random
from models.classifier import EmbedImgClassifier
from collections import OrderedDict

random_seed = 42
BATCH_SIZE = 1

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

torch.manual_seed(random_seed)
random.seed(random_seed)

images_test = get_filenames_of_path('datasets/EmbedSALUS/test/images/', '*.pt')
masks_test = get_filenames_of_path('datasets/EmbedSALUS/test/biomarkers/', '*.pt')
vessels_test = get_filenames_of_path('datasets/EmbedSALUS/test/vessels/', '*.pt')

dataset_test = EmbedLoader(images=images_test, biomarker=masks_test, vessels=vessels_test, segmentations=False, metrics=False)

dataloader_test = DataLoader(dataset_test, batch_size=BATCH_SIZE, shuffle=True)

loss_fn = torch.nn.BCEWithLogitsLoss().to(device)

model = EmbedImgClassifier()

total_metrics = {
    'accuracy': 0,
    'balanced_acc': 0,
    'specificity': 0,
    'sensitivity': 0
}

checkpoint = torch.load('outputs/classifier/embed-img/best_model.pth', map_location=torch.device("cpu"))
new_state_dict = OrderedDict()
for k, v in checkpoint.state_dict().items():
    if k.find(".fc.") == -1:
        if k[:7] == 'module.':
            name = k[7:]  # remove module.
        else:
            name = k
        new_state_dict[name] = v
model.load_state_dict(new_state_dict)
model.to(device)
model.eval()

sigmoid = torch.nn.Sigmoid()

y_true = []
y_pred = []
totalTestLoss = 0
steps = len(dataloader_test)
with torch.no_grad():
    for X, y in dataloader_test:
        X, y = X.to(device), y.to(device)
        pred = model(X)
        valid_loss = loss_fn(pred, y)
        pred = sigmoid(pred)
        pred = torch.round(pred)
        y_pred.append(pred.item())
        y_true.append(y.item())
        totalTestLoss += valid_loss.item()
tn, fp, fn, tp = confusion_matrix(y_true, y_pred, labels=[0,1]).ravel()
accuracy = (tp + tn) / (tp + fp + fn + tn)
sensitivity = tp / (tp + fn)
specificity = tn / (tn + fp)
balanced_acc = (sensitivity + specificity)/2
total_metrics['accuracy'] = accuracy
total_metrics['sensitivity'] = sensitivity
total_metrics['specificity'] = specificity
total_metrics['balanced_acc'] = balanced_acc
with open("outputs/classifier/embed-img/test_metrics.json", "w") as outfile:
    json.dump(total_metrics, outfile)
