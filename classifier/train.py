import json
import torch
from torch.utils.data import DataLoader
from sklearn.metrics import confusion_matrix
from data_loaders.image_loader import ClassificationDataSet
from util.save_best_model import SaveBestModel, save_model
from util.utils import get_filenames_of_path
import argparse
import random
from models.classifier import GlaucomaClassifier, GlaucomaResnet
from tqdm import tqdm
import logging
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Train Classifier")
parser.add_argument('-e', '--epochs', type=int, help="Number of epochs", default=100)
parser.add_argument('-m', '--metrics', help="include metrics", action='store_true')
parser.add_argument('-s', '--segmentation', help="include segmentations", action='store_true')

args = parser.parse_args()
print("img_seg=" + str(args.segmentation) + "_met=" + str(args.metrics))
BATCH_SIZE = 5
LEARNING_RATE = 1e-2
EPOCHS = args.epochs
random_seed = 42
training_size = 0.8

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")

torch.manual_seed(random_seed)
random.seed(random_seed)

images_train = get_filenames_of_path('datasets/AugSALUS/train/images/', '*.png')
masks_train = get_filenames_of_path('datasets/AugSALUS/train/biomarkers/', '*.png')
vessels_train = []
if args.segmentation:
    vessels_train = get_filenames_of_path('datasets/AugSALUS/train/vessels/', '*.png')

images_valid = get_filenames_of_path('datasets/AugSALUS/valid/images/', '*.png')
masks_valid = get_filenames_of_path('datasets/AugSALUS/valid/biomarkers/', '*.png')
vessels_valid = []
if args.segmentation:
    vessels_valid = get_filenames_of_path('datasets/AugSALUS/valid/vessels/', '*.png')


# datasets
dataset_train = ClassificationDataSet(images=images_train, biomarker=masks_train, vessels=vessels_train, segmentations=args.segmentation, metrics=args.metrics)

dataset_valid = ClassificationDataSet(images=images_valid, biomarker=masks_valid, vessels=vessels_valid, segmentations=args.segmentation, metrics=args.metrics)

dataloader_train = DataLoader(dataset_train, batch_size=BATCH_SIZE, shuffle=True)
dataloader_valid = DataLoader(dataset_valid, batch_size=1, shuffle=True)

# loss function
loss_fn = torch.nn.BCEWithLogitsLoss().to(device)

nr_features = 5
if args.segmentation and args.metrics:
    model = GlaucomaClassifier(nr_numeric_features=nr_features, nr_channels=5).to(device)
    path = "outputs/classifier/seg-met/"
elif args.segmentation:
    model = GlaucomaResnet(nr_channels=5).to(device)
    path = "outputs/classifier/seg/"
elif args.metrics:
    model = GlaucomaClassifier(nr_numeric_features=nr_features).to(device)
    path = "outputs/classifier/met/"
else:
    model = GlaucomaResnet().to(device)
    path = "outputs/classifier/img/"

model.train()
sigmoid = torch.nn.Sigmoid()
optimizer = torch.optim.Adam(model.parameters(), lr=LEARNING_RATE)

save_best_model = SaveBestModel()

S = {"train_loss": [], "test_loss": [], "test_specificity": [], "test_acc": [], "test_sensitivity": [], "test_bal_acc": []}

trainSteps = len(dataloader_train)
testSteps = len(dataloader_valid)
print("Training model...")
for e in tqdm(range(EPOCHS)):
    model.train()
    totalTrainLoss = 0
    totalTestLoss = 0
    totalTestAccuracy = 0
    valid_loss = 0
    for batch, (X, metrics, y) in enumerate(dataloader_train):
        X, metrics, y = X.to(device), metrics.to(device), y.to(device)
        optimizer.zero_grad()

        pred = model(X, metrics)
        loss = loss_fn(pred, y)

        totalTrainLoss += loss.item()

        loss.backward()
        optimizer.step()

    with torch.no_grad():
        model.eval()
        y_true = []
        y_pred = []
        for X, metrics, y in dataloader_valid:
            X, y, metrics = X.to(device), y.to(device), metrics.to(device)
            pred = model(X, metrics)
            valid_loss = loss_fn(pred, y)
            pred = sigmoid(pred)
            pred = torch.round(pred)
            y_pred.append(pred.item())
            y_true.append(y.item())
            totalTestLoss += valid_loss.item()
            # correct += (pred.argmax(1) == y).type(torch.float).sum().item()
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred, labels=[0,1]).ravel()
    avg_train_loss = totalTrainLoss / trainSteps
    avg_test_loss = totalTestLoss / testSteps
    avg_test_acc = (tp + tn) / (tp + fp + fn + tn)
    avg_test_sensitivity = tp / (tp + fn)
    avg_test_specificity = tn / (tn + fp)
    avg_test_bal_acc = (avg_test_sensitivity+avg_test_specificity)/2
    S['test_acc'].append(avg_test_acc)
    S['train_loss'].append(avg_train_loss)
    S['test_loss'].append(avg_test_loss)
    S['test_sensitivity'].append(avg_test_sensitivity)
    S['test_specificity'].append(avg_test_specificity)
    S['test_bal_acc'].append(avg_test_bal_acc)
    logging.info(f"EPOCH: {e+1}/{EPOCHS}")
    logging.info("Train loss: {:.6f}, Validation loss: {:.6f}, Balanced accuracy: {:.2f}".format(
        avg_train_loss, avg_test_loss, avg_test_bal_acc
    ))
    print(f"EPOCH: {e+1}/{EPOCHS}")
    print("Train loss: {:.6f}, Validation loss: {:.6f}".format(
        avg_train_loss, avg_test_loss
    ))
    save_best_model(avg_test_loss, e, model, optimizer, loss_fn, path)
save_model(EPOCHS, model, optimizer, loss_fn, path)
print("Done!")
mode = "img_seg=" + str(args.segmentation) + "_met=" + str(args.metrics)
plt.style.use("ggplot")
plt.figure()
plt.plot(S['train_loss'], label="Train loss")
plt.plot(S['test_loss'], label="Validation loss")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend(loc="lower left")
plt.savefig('graphs/{}_classifier_train_loss.png'.format(mode))

# Graph for IOU score and accuracy
plt.style.use("ggplot")
plt.figure()
plt.plot(S['test_acc'], label="Validation accuracy")
plt.plot(S['test_specificity'], label="Validation specificity")
plt.plot(S['test_sensitivity'], label="Validation sensitivity")
plt.plot(S['test_bal_acc'], label="Validation balanced accuracy")
plt.xlabel("Epochs")
plt.ylabel("%")
plt.legend(loc="lower left")
plt.savefig('graphs/{}_classifier_train_metrics.png'.format(mode))
with open(path+"metrics.json", "w") as outfile:
    json.dump(S, outfile)
