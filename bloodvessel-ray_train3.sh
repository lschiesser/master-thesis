#!/bin/bash -l

####################
# Some directives  #
####################


# Something to identify your job
#SBATCH --job-name=ray-bloodvessel_training

# One node of available imi-ml-[1,2,3]
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --gpus-per-task=1

# In case of Multi-Tasks you can specify CPUs per task
# Multitask has to be using MPI standards!
# If you are not familiar with that it's most likely the wrong setting
#SBATCH --ntasks-per-node=1
##SBATCH --ntasks-per-node=4

# Estimated Time - Consistently underestimating results in penalties
#SBATCH --time=0-04:00:00

# Currently only 1 Partition exists (namely test)
#SBATCH --partition gpu

# RAM per CPU-Core instance or total. In this case total is used
##SBATCH --mem-per-cpu=100MB
#SBATCH --mem=32GB

# Whether/When a Mail should be send to given address
#SBATCH --mail-type=END,FAIL


################
# Actual Job   #
################

echo 'Starting Job '$SLURM_JOB_ID
echo $(date +'%d.%m.%Y %H:%M')
echo 'Ray train 3 blood vessel model'

# Change your WD
cd /media/gluster/schiesser/master-thesis
# Activate Py-Env with required modules installed
source ./env/bin/activate

# Run the script
python3 -m bloodvessel_extraction.ray_train3

echo "Finished training..."

exit 0